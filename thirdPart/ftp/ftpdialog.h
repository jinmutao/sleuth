﻿#ifndef FTPDIALOG_H
#define FTPDIALOG_H

#include <qftp.h>

#include <QDebug>
#include <QDialog>
#include <QFileDialog>
#include <QHash>
#include <QMenu>
#include <QString>

#include "loadingform.h"
#include "sleuthlib/utils.h"
namespace Ui {
class FtpDialog;
}

class FtpDialog : public QDialog {
    Q_OBJECT

public:
    explicit FtpDialog(QWidget *parent = nullptr);
    ~FtpDialog();

    void showLoadingForm();
    void hideLoadingForm();

    QString ToSpecialEncoding(const QString &InputStr);
    QString FromSpecialEncoding(const QString &InputStr);

protected:
    virtual void resizeEvent(QResizeEvent *event) override;
    virtual void contextMenuEvent(QContextMenuEvent *event) override;

private slots:
    void on_btn_connect_clicked();
    void commandFinished(int, bool err);
    void listInfo(QUrlInfo url);

    void on_tw_ftpContent_doubleClicked(const QModelIndex &index);
    void onRefresh();
    void onDownload();
    void dataTransferProgress(qint64, qint64);

    void on_btn_disconnect_clicked();

private:
    Ui::FtpDialog *ui;
    QWidget *      m_pMaskLayer = nullptr;
    QFtp           ftp;

    QHash<QString, QString> listType;
    QHash<QString, bool>    listPath;
    QString                 currentPath;
    QFile                   file;
    QStringList             typeOptions;
    QStringList             pendingDirs;

    // ===== function ====
    QIcon getFileIconByType(QString fileName, QString &type);
    void  clear();
};

#endif  // FTPDIALOG_H
