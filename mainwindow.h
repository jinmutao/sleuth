﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * @projectName   msleuth
 * @brief         摘要
 * @author        cmk
 * @date          2020-10-15
 */

#include <QMainWindow>
#include <QProcess>

#include "scenarioform.h"
#include "thirdPart/ftp/ftpdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString getScenarioPath();

private slots:

    void scenarioForm2Main();

    void on_rb_test_clicked();

    void on_rb_predict_clicked();

    void on_rb_restart_clicked();

    void on_rb_calibrate_clicked();

    void on_btn_startSimulation_clicked();

    void setRes2Te();

    void on_btn_scenario_clicked();

    void on_btn_stopSimulation_clicked();

    void on_tab2_btn_file_clicked();

    void on_btn_editScenario_clicked();

    void slotProcessFinshed();

    void showFTPDialog();

private:
    Ui::MainWindow *ui;

    QString       mode;
    QString       scenarioPath;
    QProcess *    m_process;
    ScenarioForm *scenarioForm;
    /**
     * @brief correctOutputDir
     * 验证输出目录的合法性，并修正输出目录
     */
    void correctOutputDir();
};
#endif  // MAINWINDOW_H
