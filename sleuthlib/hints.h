#ifndef HINTS_H
#define HINTS_H
#include <QString>
#include <QStringList>
////
/// contents for hints
///
namespace MHINTS {
const QString PREVIEW_ALL_HINTS = R"(# FILE: 'scenario file' for SLEUTH land cover transition model
#       (UGM  v3.0)
#       Comments start with #
#
#   I. Path Name Variables
#  II. Running Status (Echo)
# III. Output ASCII Files
#  IV. Log File Preferences
#   V. Working Grids
#  VI. Random Number Seed
# VII. Monte Carlo Iteration
#VIII. Coefficients
#      A. Coefficients and Growth Types
#      B. Modes and Coefficient Settings
#  IX. Prediction Date Range
#   X. Input Images
#  XI. Output Images
# XII. Colortable Settings
#      A. Date_Color
#      B. Non-Landuse Colortable
#      C. Land Cover Colortable
#      D. Growth Type Images
#      E. Deltatron Images
#XIII. Self Modification Parameters
)";

const QString I_PATH_NAME_VARIABLES_HINTS = R"(# I.PATH NAME VARIABLES
#   INPUT_DIR: relative or absolute path where input image files and
#              (if modeling land cover) 'landuse.classes' file are
#              located.
#   OUTPUT_DIR: relative or absolute path where all output files will
#               be located.
#   WHIRLGIF_BINARY: relative path to 'whirlgif' gif animation program.
#                    These must be compiled before execution.
)";

const QString II_RUNNING_STATUS__HINTS = R"(# II. RUNNING STATUS (ECHO)
#  Status of model run, monte carlo iteration, and year will be
#  printed to the screen during model execution.
)";

const QString III_Output_Files_HINTS = R"(# III. Output Files
# INDICATE TYPES OF ASCII DATA FILES TO BE WRITTEN TO OUTPUT_DIRECTORY.
#
#   COEFF_FILE: contains coefficient values for every run, monte carlo
#               iteration and year.
#   AVG_FILE: contains measured values of simulated data averaged over
#             monte carlo iterations for every run and control year.
#   STD_DEV_FILE: contains standard diviation of averaged values
#                 in the AVG_FILE.
#   MEMORY_MAP: logs memory map to file 'memory.log'
#   LOGGING: will create a 'LOG_#' file where # signifies the processor
#            number that created the file if running code in parallel.
#            Otherwise, # will be 0. Contents of the LOG file may be
#            described below.
)";

const QString IV_Log_File_Preferences_HINTS = R"(# IV. Log File Preferences
# INDICATE CONTENT OF LOG_# FILE (IF LOGGING == ON).
#   LANDCLASS_SUMMARY: (if landuse is being modeled) summary of input
#                      from 'landuse.classes' file
#   SLOPE_WEIGHTS(YES/NO): annual slope weight values as effected
#                          by slope_coeff
#   READS(YES/NO)= notes if a file is read in
#   WRITES(YES/NO)= notes if a file is written
#   COLORTABLES(YES/NO)= rgb lookup tables for all colortables generated
#   PROCESSING_STATUS(0:off/1:low verbosity/2:high verbosity)=
#   TRANSITION_MATRIX(YES/NO)= pixel count and annual probability of
#                              land class transitions
#   URBANIZATION_ATTEMPTS(YES/NO)= number of times an attempt to urbanize
#                                  a pixel occurred
#   INITIAL_COEFFICIENTS(YES/NO)= initial coefficient values for
#                                 each monte carlo
#   BASE_STATISTICS(YES/NO)= measurements of urban control year data
#   DEBUG(YES/NO)= data dump of igrid object and grid pointers
#   TIMINGS(0:off/1:low verbosity/2:high verbosity)= time spent within
#     each module. If running in parallel, LOG_0 will contain timing for
#     complete job.
)";

const QString V_WORKING_GRIDS_HINTS = R"(# V. WORKING GRIDS
# The number of working grids needed from memory during model execution is
# designated up front. This number may change depending upon modes. If
# NUM_WORKING_GRIDS needs to be increased, the execution will be exited
# and an error message will be written to the screen and to 'ERROR_LOG'
# in the OUTPUT_DIRECTORY. If the number may be decreased an optimal
# number will be written to the end of the LOG_0 file.
)";

const QString VI_RANDOM_NUMBER_SEED_HINTS = R"(# VI. RANDOM NUMBER SEED
# This number initializes the random number generator. This seed will be
# used to initialize each model run.
)";

const QString VII_MONTE_CARLO_ITERATIONS_HINTS = R"(# VII. MONTE CARLO ITERATIONS
# Each model run may be completed in a monte carlo fashion.
#  For CALIBRATION or TEST mode measurements of simulated data will be
#  taken for years of known data, and averaged over the number of monte
#  carlo iterations. These averages are written to the AVG_FILE, and
#  the associated standard diviation is written to the STD_DEV_FILE.
#  The averaged values are compared to the known data, and a Pearson
#  correlation coefficient measure is calculated and written to the
#  control_stats.log file. The input per run may be associated across
#  files using the 'index' number in the files' first column.
)";

const QString VIII_COEFFICIENTS_HINTS = R"(# VIII. COEFFICIENTS
# The coefficients effect how the growth rules are applied to the data.
# Setting requirements:
#    *_START values >= *_STOP values
#    *_STEP values > 0
#   if no coefficient increment is desired:
#    *_START == *_STOP
#    *_STEP == 1
# For additional information about how these values affect simulated
# land cover change see our publications and PROJECT GIGALOPOLIS
#  site: (www.ncgia.ucsb.edu/project/gig/About/abGrowth.htm).
#  A. COEFFICIENTS AND GROWTH TYPES
#     DIFFUSION: affects SPONTANEOUS GROWTH and search distance along the
#                road network as part of ROAD INFLUENCED GROWTH.
#     BREED: NEW SPREADING CENTER probability and affects number of ROAD
#            INFLUENCED GROWTH attempts.
#     SPREAD: the probabilty of ORGANIC GROWTH from established urban
#             pixels occuring.
#     SLOPE_RESISTANCE: affects the influence of slope to urbanization. As
#                       value increases, the ability to urbanize
#                       ever steepening slopes decreases.
#     ROAD_GRAVITY: affects the outward distance from a selected pixel for
#                   which a road pixel will be searched for as part of
#                   ROAD INFLUENCED GROWTH.
#
#  B. MODES AND COEFFICIENT SETTINGS
#     TEST: TEST mode will perform a single run through the historical
#           data using the CALIBRATION_*_START values to initialize
#           growth, complete the MONTE_CARLO_ITERATIONS, and then conclude
#           execution. GIF images of the simulated urban growth will be
#           written to the OUTPUT_DIRECTORY.
#     CALIBRATE: CALIBRATE will perform monte carlo runs through the
#                historical data using every combination of the
#                coefficient values indicated. The CALIBRATION_*_START
#                coefficient values will initialize the first run. A
#                coefficient will then be increased by its *_STEP value,
#                and another run performed. This will be repeated for all
#                possible permutations of given ranges and increments.
#     PREDICTION: PREDICTION will perform a single run, in monte carlo
#                 fashion, using the PREDICTION_*_BEST_FIT values
#                 for initialization.
)";

const QString IX_PREDICTION_DATE_RANGE_HINTS = R"(# IX. PREDICTION DATE RANGE
# The urban and road images used to initialize growth during
# prediction are those with dates equal to, or greater than,
# the PREDICTION_START_DATE. If the PREDICTION_START_DATE is greater
# than any of the urban dates, the last urban file on the list will be
# used. Similarly, if the PREDICTION_START_DATE is greater
# than any of the road dates, the last road file on the list will be
# used. The prediction run will terminate at PREDICTION_STOP_DATE.
)";

const QString X_INPUT_IMAGES_HINTS = R"(# X. INPUT IMAGES
# The model expects grayscale, GIF image files with file name
# format as described below. For more information see our
# PROJECT GIGALOPOLIS web site:
# (www.ncgia.ucsb.edu/project/gig/About/dtInput.htm).
#
# IF LAND COVER IS NOT BEING MODELED: Remove or comment out
# the LANDUSE_DATA data input flags below.
#
#    <  >  = user selected fields
#   [<  >] = optional fields
#
# Urban data GIFs
#  format:  <location>.urban.<date>.[<user info>].gif
# URBAN_DATA= demo200.urban.1930.gif
#
# Road data GIFs
#  format:  <location>.roads.<date>.[<user info>].gif
# ROAD_DATA= demo200.roads.1930.gif
#
# Landuse data GIFs
#  format:  <location>.landuse.<date>.[<user info>].gif
# LANDUSE_DATA= demo200.landuse.1930.gif
#
# Excluded data GIF
#  format:  <location>.excluded.[<user info>].gif
# EXCLUDED_DATA= demo200.excluded.gif
#
# Slope data GIF
#  format:  <location>.slope.[<user info>].gif
# SLOPE_DATA= demo200.slope.gif
#
# Background data GIF
#  format:   <location>.hillshade.[<user info>].gif
# BACKGROUND_DATA= demo200.hillshade.gif
)";

const QString XI_OUTPUT_IMAGES_HINTS = R"(# XI. OUTPUT IMAGES
#   WRITE_COLOR_KEY_IMAGES: Creates image maps of each colortable.
#                           File name format: 'key_[type]_COLORMAP'
#                           where [type] represents the colortable.
#   ECHO_IMAGE_FILES: Creates GIF of each input file used in that job.
#                     File names format: 'echo_of_[input_filename]'
#                     where [input_filename] represents the input name.
#   ANIMATION: if whirlgif has been compiled, and the WHIRLGIF_BINARY
#              path has been defined, animated gifs begining with the
#              file name 'animated' will be created in PREDICT mode.
)";

const QString XII_COLORTABLE_SETTINGS_HINTS = R"(# XII. COLORTABLE SETTINGS
#  A. DATE COLOR SETTING
#     The date will automatically be placed in the lower left corner
#     of output images. DATE_COLOR may be designated in with red, green,
#     and blue values (format: <red_value, green_value, blue_value> )
#     or with hexadecimal begining with '0X' (format: <0X######> ).
# -----------------------------------------------------------------
#  B. URBAN (NON-LANDUSE) COLORTABLE SETTINGS
#     1. URBAN MODE OUTPUTS
#         TEST mode: Annual images of simulated urban growth will be
#                    created using SEED_COLOR to indicate urbanized areas.

#         CALIBRATE mode: Images will not be created.
#         PREDICT mode: Annual probability images of simulated urban
#                       growth will be created using the PROBABILITY
#                       _COLORTABLE. The initializing urban data will be
#                       indicated by SEED_COLOR.
#
#     2. COLORTABLE SETTINGS
#          SEED_COLOR: initializing and extrapolated historic urban extent

#          WATER_COLOR: BACKGROUND_DATA is used as a backdrop for

#                       simulated urban growth. If pixels in this file
#                       contain the value zero (0), they will be filled
#                       with the color value in WATER_COLOR. In this way,
#                       major water bodies in a study area may be included
#                       in output images.
# default
# DATE_COLOR= 0XFFFFFF white
# SEED_COLOR= 0XFFFF00 #yellow
# WATER_COLOR=  0X0000FF # blue
)";

const QString XII3_PROBABILITY_COLOR_HINTS = R"(#     3. PROBABILITY COLORTABLE FOR URBAN GROWTH
#        For PREDICTION, annual probability images of urban growth
#        will be created using the monte carlo iterations. In these
#        images, the higher the value the more likely urbanizaion is.
#        In order to interpret these 'continuous' values more easily
#        they may be color classified by range.
#
#        If 'hex' is not present then the range is transparent.
#        The transparent range must be the first on the list.
#        The max number of entries is 100.
#          PROBABILITY_COLOR: a color value in hexadecimal that indicates
#                             a probability range.
#            low/upper: indicate the boundaries of the range.
#                  low,  upper,   hex,  (Optional Name)
)";

const QString XIIC_LANDCOLOR_HINTS = R"(#  C. LAND COVER COLORTABLE
#  Land cover input images should be in grayscale GIF image format.
#  The 'pix' value indicates a land class grayscale pixel value in
#  the image. If desired, the model will create color classified
#  land cover output. The output colortable is designated by the
#  'hex/rgb' values.
#    pix: input land class pixel value
#    name: text string indicating land class
#    flag: special case land classes
#          URB - urban class (area is included in urban input data
#                and will not be transitioned by deltatron)
#          UNC - unclass (NODATA areas in image)
#          EXC - excluded (land class will be ignored by deltatron)
#    hex/rgb: hexidecimal or rgb (red, green, blue) output colors
#                      pix,    name,      flag,   hex/rgb, #comment
)";

const QString XIID_GROWTHCOLOR_HINTS = R"(#  D. GROWTH TYPE IMAGE OUTPUT CONTROL AND COLORTABLE
#
#  From here you can control the output of the Z grid
#  (urban growth) just after it is returned from the spr_spread()
#  function. In this way it is possible to see the different types
#  of growth that have occured for a particular growth cycle.
#
#  VIEW_GROWTH_TYPES(YES/NO) provides an on/off
#  toggle to control whether the images are generated.
#
#  GROWTH_TYPE_PRINT_WINDOW provides a print window
#  to control the amount of images created.
#  format:  <start_run>,<end_run>,<start_monte_carlo>,
#           <end_monte_carlo>,<start_year>,<end_year>
#  for example:
#  GROWTH_TYPE_PRINT_WINDOW=run1,run2,mc1,mc2,year1,year2
#  so images are only created when
#  run1<= current run <=run2 AND
#  mc1 <= current monte carlo <= mc2 AND
#  year1 <= currrent year <= year2
#
#  0 == first
)";

const QString XIIE_DELTATRONCOLOR_HINTS = R"(#  E. DELTATRON AGING SECTION
#
#  From here you can control the output of the deltatron grid
#  just before they are aged
#
#  VIEW_DELTATRON_AGING(YES/NO) provides an on/off
#  toggle to control whether the images are generated.
#
#  DELTATRON_PRINT_WINDOW provides a print window
#  to control the amount of images created.
#  format:  <start_run>,<end_run>,<start_monte_carlo>,
#           <end_monte_carlo>,<start_year>,<end_year>
#  for example:
#  DELTATRON_PRINT_WINDOW=run1,run2,mc1,mc2,year1,year2
#  so images are only created when
#  run1<= current run <=run2 AND
#  mc1 <= current monte carlo <= mc2 AND
#  year1 <= currrent year <= year2
#
#  0 == first
)";

const QString XII_SELF_MODIFICATION_HINTS = R"(# XIII. SELF-MODIFICATION PARAMETERS
#       SLEUTH is a self-modifying cellular automata. For more
#       information see our PROJECT GIGALOPOLIS web site
#       (www.ncgia.ucsb.edu/project/gig/About/abGrowth.htm)
#       and publications (and/or grep 'self modification' in code).
)";

}  // namespace MHINTS

namespace TEMFILES {
const QStringList calFiles    = {"control_stats.log", "LOG_0", "memory.log", "restart_file.data0"};
const QStringList calFilePath = {"./templateFiles/control_stats.log", "./templateFiles/LOG_0",
    "./templateFiles/memory.log", "./templateFiles/restart_file.data0"};

const QStringList preFiles = {"key_DELTATRON_COLORMAP.gif", "LOG_0", "key_GROWTH_COLORMAP.gif",
    "key_LANDUSE_COLORMAP.gif", "key_PROBABILITY_COLORMAP.gif"};
const QStringList preFilePath
    = {"./templateFiles/key_DELTATRON_COLORMAP.gif", "./templateFiles/LOG_0", "./templateFiles/key_GROWTH_COLORMAP.gif",
        "./templateFiles/key_LANDUSE_COLORMAP.gif", "key_PROBABILITY_COLORMAP.gif"};

const QStringList testFiles    = {"LOG_0"};
const QStringList testFilePath = {"./templateFiles/LOG_0"};
}  // namespace TEMFILES
#endif  // HINTS_H
