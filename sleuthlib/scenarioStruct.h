#ifndef SCENARIOSTRUCT_H
#define SCENARIOSTRUCT_H

#include <QColor>
#include <QDebug>
#include <QString>
#include <QStringList>

#include "utils.h"

struct ProbablityColor {
    int    low;
    int    high;
    bool   isTransparent;
    QColor color;

    ProbablityColor() {
        low           = 0;
        high          = 50;
        color         = QColor(255, 255, 255);
        isTransparent = false;
    }
    QString toString() {
        int     fieldWidth = 8;
        QString outStr     = QString("PROBABILITY_COLOR= %1,%2, %3,")
                             .arg(low, fieldWidth)
                             .arg(high, fieldWidth)
                             .arg(isTransparent ? "        " : MYUTILS::converRGB16HexStr(color), fieldWidth);
        return outStr;
    }
};

struct PrintWindow {
    int startRun;
    int endRun;
    int startMonteCarlo;
    int endMonteCarlo;
    int startYear;
    int endYear;

    PrintWindow() {
        startRun        = 0;
        endRun          = 0;
        startMonteCarlo = 0;
        endMonteCarlo   = 0;
        startYear       = 1995;
        endYear         = 2020;
    }

    QString toString() {
        int     fielsWidth = 5;
        QString outStr     = QString("%1,%2,%3,%4,%5,%6")
                             .arg(startRun, fielsWidth)
                             .arg(endRun, fielsWidth)
                             .arg(startMonteCarlo, fielsWidth)
                             .arg(endMonteCarlo, fielsWidth)
                             .arg(startYear, fielsWidth)
                             .arg(endYear, fielsWidth);
        return outStr;
    }
};

enum LOGENUM
{
    _off_,
    _low_verbosity_,
    _high_verbosity_,

};

struct LogsAndOutputsControl {
    bool ECHO;
    ///////  III. Output Files ////////
    bool WRITE_COEFF_FILE;
    bool WRITE_AVG_FILE;
    bool WRITE_STD_DEV_FILE;
    bool WRITE_MEMORY_MAP;
    bool LOGGING;

    //////  IV. Log File Preferences ///////
    bool LOG_LANDCLASS_SUMMARY;
    bool LOG_SLOPE_WEIGHTS;
    bool LOG_READS;
    bool LOG_WRITES;
    bool LOG_COLORTABLES;
    // 0:off/1:low verbosity/2:high verbosity
    enum LOGENUM LOG_PROCESSING_STATUS;
    bool         LOG_TRANSITION_MATRIX;
    bool         LOG_URBANIZATION_ATTEMPTS;
    bool         LOG_INITIAL_COEFFICIENTS;
    bool         LOG_BASE_STATISTICS;
    bool         LOG_DEBUG;
    // (0:off/1:low verbosity/2:high verbosity)
    enum LOGENUM LOG_TIMINGS;

    ////// XI. OUTPUT IMAGES  ////////
    bool WRITE_COLOR_KEY_IMAGES;
    bool ECHO_IMAGE_FILES;
    bool ANIMATION;

    ///// color table ////
    bool VIEW_GROWTH_TYPES;
    bool VIEW_DELTATRON_AGING;

    ///// print window ////
    PrintWindow DELTATRON_PRINT_WINDOW;
    PrintWindow GROWTH_TYPE_PRINT_WINDOW;

    LogsAndOutputsControl() {
        ECHO               = true;
        WRITE_COEFF_FILE   = true;
        WRITE_AVG_FILE     = true;
        WRITE_STD_DEV_FILE = true;
        WRITE_MEMORY_MAP   = true;
        LOGGING            = true;

        LOG_LANDCLASS_SUMMARY     = true;
        LOG_SLOPE_WEIGHTS         = false;
        LOG_READS                 = false;
        LOG_WRITES                = false;
        LOG_COLORTABLES           = false;
        LOG_PROCESSING_STATUS     = _off_;
        LOG_TRANSITION_MATRIX     = true;
        LOG_URBANIZATION_ATTEMPTS = false;
        LOG_INITIAL_COEFFICIENTS  = false;
        LOG_BASE_STATISTICS       = true;
        LOG_DEBUG                 = true;
        LOG_TIMINGS               = _off_;

        WRITE_COLOR_KEY_IMAGES = true;
        ECHO_IMAGE_FILES       = true;
        ANIMATION              = true;

        VIEW_GROWTH_TYPES    = false;
        VIEW_DELTATRON_AGING = false;

        DELTATRON_PRINT_WINDOW.startYear = 1930;
    }
};

struct CoefficientsInfo {
    int     start;
    int     step;
    int     stop;
    QString cname;
    QString toString() {
        QString sta    = "CALIBRATION_%1_START= %2";
        QString ste    = "CALIBRATION_%1_STEP=  %2";
        QString sto    = "CALIBRATION_%1_STOP=  %2\n";
        QString outStr = sta.arg(cname).arg(start) + "\n" + ste.arg(cname).arg(step) + "\n" + sto.arg(cname).arg(stop);
        return outStr;
    }
};

struct BaseCoefficients {
    double  diffusion;
    double  breed;
    double  spread;
    double  slope;
    double  road;
    QString toString() {
        QString outStr(R"(PREDICTION_DIFFUSION_BEST_FIT=  %1
PREDICTION_BREED_BEST_FIT=  %2
PREDICTION_SPREAD_BEST_FIT=  %3
PREDICTION_SLOPE_BEST_FIT=  %4
PREDICTION_ROAD_BEST_FIT=  %5)");
        return outStr.arg(diffusion).arg(breed).arg(spread).arg(slope).arg(road);
    }
};

struct Coefficients {
    CoefficientsInfo diffusion;
    CoefficientsInfo breed;
    CoefficientsInfo spread;
    CoefficientsInfo slope;
    CoefficientsInfo road;
    BaseCoefficients bestFit;
    Coefficients() {
        diffusion.start = 5;
        diffusion.step  = 1;
        diffusion.stop  = 5;
        diffusion.cname = "DIFFUSION";

        breed.start = 5;
        breed.step  = 1;
        breed.stop  = 5;
        breed.cname = "BREED";

        spread.start = 10;
        spread.step  = 1;
        spread.stop  = 10;
        spread.cname = "SPREAD";

        slope.start = 95;
        slope.step  = 1;
        slope.step  = 95;
        slope.cname = "SLOPE";

        road.start = 5;
        road.step  = 1;
        road.stop  = 5;
        road.cname = "ROAD";

        bestFit.diffusion = 20;
        bestFit.breed     = 20;
        bestFit.spread    = 20;
        bestFit.slope     = 20;
        bestFit.road      = 20;
    }
};

// struct Coefficients {
//    int CALIBRATION_DIFFUSION_START;
//    int CALIBRATION_DIFFUSION_STEP;
//    int CALIBRATION_DIFFUSION_STOP;
//    int CALIBRATION_BREED_START;
//    int CALIBRATION_BREED_STEP;
//    int CALIBRATION_BREED_STOP;
//    int CALIBRATION_SPREAD_START;
//    int CALIBRATION_SPREAD_STEP;
//    int CALIBRATION_SPREAD_STOP;
//    int CALIBRATION_SLOPE_START;
//    int CALIBRATION_SLOPE_STEP;
//    int CALIBRATION_SLOPE_STOP;
//    int CALIBRATION_ROAD_START;
//    int CALIBRATION_ROAD_STEP;
//    int CALIBRATION_ROAD_STOP;
//    int PREDICTION_DIFFUSION_BEST_FIT;
//    int PREDICTION_BREED_BEST_FIT;
//    int PREDICTION_SPREAD_BEST_FIT;
//    int PREDICTION_SLOPE_BEST_FIT;
//    int PREDICTION_ROAD_BEST_FIT;
//    Coefficients() {
//        CALIBRATION_DIFFUSION_START = 5;
//        CALIBRATION_DIFFUSION_STEP  = 1;
//        CALIBRATION_DIFFUSION_STOP  = 5;
//        CALIBRATION_BREED_START     = 5;
//        CALIBRATION_BREED_STEP      = 1;
//        CALIBRATION_BREED_STOP      = 5;

//        CALIBRATION_SPREAD_START = 10;
//        CALIBRATION_SPREAD_STEP  = 1;
//        CALIBRATION_SPREAD_STOP  = 10;

//        CALIBRATION_SLOPE_START = 95;
//        CALIBRATION_SLOPE_STEP  = 1;
//        CALIBRATION_SLOPE_STOP  = 95;

//        CALIBRATION_ROAD_START = 5;
//        CALIBRATION_ROAD_STEP  = 1;
//        CALIBRATION_ROAD_STOP  = 5;

//        PREDICTION_DIFFUSION_BEST_FIT = 20;
//        PREDICTION_BREED_BEST_FIT     = 20;
//        PREDICTION_SPREAD_BEST_FIT    = 20;
//        PREDICTION_SLOPE_BEST_FIT     = 20;
//        PREDICTION_ROAD_BEST_FIT      = 20;
//    }
//};

struct PredictionDateRange {
    int PREDICTION_START_DATE;
    int PREDICTION_STOP_DATE;
    PredictionDateRange() {
        PREDICTION_STOP_DATE  = 2010;
        PREDICTION_START_DATE = 1990;
    }
    QString toString() {
        return QString(R"(
PREDICTION_START_DATE=%1
PREDICTION_STOP_DATE=%2
          )")
            .arg(PREDICTION_START_DATE)
            .arg(PREDICTION_STOP_DATE);
    }
};

struct InputImageFiles {
    QStringList URBAN_DATAS;
    QStringList ROAD_DATAS;
    QStringList LANDUSE_DATA;
    QString     EXCLUDED_DATA;
    QString     SLOPE_DATA;
    QString     BACKGROUND_DATA;
};

struct LandClass {
    int     pix;
    QString name;
    QString flag;
    QColor  color;
    LandClass() {
        pix   = 0;
        name  = "Unclass";
        flag  = "UNC";
        color = QColor(255, 255, 255);
    }
    QString toString() {
        int     fieldWidth = 10;
        QString outStr     = QString("LANDUSE_CLASS=%1,%2,%3,%4")
                             .arg(pix, fieldWidth)
                             .arg(name, fieldWidth)
                             .arg(flag == "" ? "\t" : flag, fieldWidth)
                             .arg(MYUTILS::converRGB16HexStr(color), fieldWidth);
        return outStr;
    }
};

struct PhaseGrowthColor {
    QList<QColor> pColors;
};

struct DeltatronColor {
    QColor dColors;
};

struct SelfModfParams {
    double ROAD_GRAV_SENSITIVITY;
    double SLOPE_SENSITIVITY;
    double CRITICAL_LOW;
    double CRITICAL_HIGH;
    double CRITICAL_SLOPE;
    double BOOM;
    double BUST;

    SelfModfParams() {
        ROAD_GRAV_SENSITIVITY = 0.01;
        SLOPE_SENSITIVITY     = 0.1;
        CRITICAL_LOW          = 0.97;
        CRITICAL_HIGH         = 1.3;
        CRITICAL_SLOPE        = 21.0;
        BOOM                  = 1.01;
        BUST                  = 0.9;
    }
    QString toString() {
        QString outStr(R"(
ROAD_GRAV_SENSITIVITY=%1
SLOPE_SENSITIVITY=%2
CRITICAL_LOW=%3
CRITICAL_HIGH=%4
#CRITICAL_LOW=0.0
#CRITICAL_HIGH=10000000000000.0
CRITICAL_SLOPE=%5
BOOM=%6
BUST=%7 )");
        return outStr.arg(ROAD_GRAV_SENSITIVITY)
            .arg(SLOPE_SENSITIVITY)
            .arg(CRITICAL_LOW)
            .arg(CRITICAL_HIGH)
            .arg(CRITICAL_SLOPE)
            .arg(BOOM)
            .arg(BUST);
    }
};
#endif  // SCENARIOSTRUCT_H
