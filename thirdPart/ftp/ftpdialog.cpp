﻿#include "ftpdialog.h"

#include "ui_ftpdialog.h"

FtpDialog::FtpDialog(QWidget *parent) : QDialog(parent), ui(new Ui::FtpDialog), ftp(0) {
    ui->setupUi(this);
    ui->le_ip->setText(MYUTILS::ftpIp);
    ui->le_account->setText(MYUTILS::userName);
    ui->le_passwrod->setText(MYUTILS::userPasswd);
    ui->le_port->setText(MYUTILS::port);
    typeOptions.append({"zip", "rar", "exe"});

    m_pMaskLayer = new LoadingForm(this);
    m_pMaskLayer->setFixedSize(this->size());  //设置窗口大小
    m_pMaskLayer->setVisible(false);           //初始状态下隐藏，待需要显示时使用
    this->stackUnder(m_pMaskLayer);            //其中pWrapper为当前窗口的QWidget

    //    ui->le_port->setText("21");
    //    ui->le_ip->setText("192.168.252.1");
    //    ui->le_account->setText("cmk");
    //    ui->le_passwrod->setText("123");
    // 设置表格列宽
    ui->tw_ftpContent->setColumnCount(4);
    ui->tw_ftpContent->setHorizontalHeaderLabels({QString::fromLocal8Bit("名称"), QString::fromLocal8Bit("日期"),
        QString::fromLocal8Bit("类型"), QString::fromLocal8Bit("大小")});
    ui->tw_ftpContent->verticalHeader()->setVisible(false);
    for (int i = 0; i < 4; ++i) {
        ui->tw_ftpContent->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }

    // 去除选中的虚线框
    //    ui->tw_ftpContent->setItemDelegate((QStyledItemDelegate *)new MyStyledItemDelegate);

    // 设置进度条
    ui->progressBar->hide();

    // 信号槽
    connect(&ftp, SIGNAL(listInfo(QUrlInfo)), SLOT(listInfo(QUrlInfo)));
    connect(&ftp, SIGNAL(commandFinished(int, bool)), SLOT(commandFinished(int, bool)));
    connect(&ftp, SIGNAL(dataTransferProgress(qint64, qint64)), SLOT(dataTransferProgress(qint64, qint64)));
}

FtpDialog::~FtpDialog() { delete ui; }

void FtpDialog::on_btn_connect_clicked() {
    QString serverAddress = ui->le_ip->text();
    if (serverAddress.isEmpty()) {
        ui->label_status->setText(QString::fromLocal8Bit("服务器地址为空!"));
        return;
    }

    QString port = ui->le_port->text();
    if (port.isEmpty()) {
        ui->label_status->setText(QString::fromLocal8Bit("端口号为空!"));
        return;
    }

    QString account  = ui->le_account->text();
    QString password = ui->le_passwrod->text();

    // 如果已经登录了就不需要重复登录
    if (ftp.state() != QFtp::LoggedIn) {
        currentPath = "/";
        ftp.connectToHost(serverAddress, port.toInt());
        ftp.login(account, password);

        showLoadingForm();
    }

    //    // 保存到配置文件
    //    saveToIni();
}

void FtpDialog::commandFinished(int, bool err) {
    int     cmd = ftp.currentCommand();
    QString msg;
    hideLoadingForm();
    //    ftp.setTransferMode(QFtp::TransferMode::Passive);
    qDebug() << "cmd--" << cmd;
    switch (cmd) {
        case QFtp::Login:
            if (!err) {
                ftp.list();  // 成功则显示列表
                qDebug() << ftp.state();
            }
            msg = err ? ftp.errorString() : QString::fromLocal8Bit("服务器连接成功!");
            ui->label_status->setText(msg);
            break;

        case QFtp::Close:
            if (!err) {
                clear();
                currentPath.clear();
            }  // 清除列表
            msg = err ? ftp.errorString() : QString::fromLocal8Bit("断开服务器连接!");
            ui->label_status->setText(msg);
            break;

        case QFtp::Get:
            if (file.isOpen()) {
                file.flush();
                file.close();
            }
            ui->label_status->setText(err ? ftp.errorString() : QString::fromLocal8Bit("文件下载成功!"));
            break;

        case QFtp::Put:
            //        if (file.isOpen()) file.close();
            //        if (!err) onInsertRow();
            ui->label_status->setText(err ? ftp.errorString() : "文件上传成功!");
            break;

        case QFtp::Rename:
            //        if (!createFolder) statusBar()->showMessage(err ? ftp.errorString() : "重命名成功!", 2000);
            //        createFolder = false;
            break;

        case QFtp::Remove:
            //        if (!err) ui->tableWidget->removeRow(removeRow);
            ui->label_status->setText(err ? ftp.errorString() : "删除文件成功!");
            break;

        case QFtp::Mkdir:
            ui->label_status->setText(err ? ftp.errorString() : "创建文件夹成功!");
            break;

        case QFtp::Rmdir:
            //                    if (!err) ui->tableWidget->removeRow(removeRow);
            ui->label_status->setText(err ? ftp.errorString() : QString::fromLocal8Bit("删除文件夹成功!"));
            break;
        case QFtp::None:
            ui->label_status->setText(QString::fromLocal8Bit("连接错误!"));
    }
}

void FtpDialog::listInfo(QUrlInfo url) {
    // 解决中文乱码问题
    QString name = QString::fromLocal8Bit(url.name().toLatin1());
    QString type;
    QIcon   icon;
    if (url.isDir()) {
        type = QString::fromLocal8Bit("目录");
        icon = QIcon(":/ftp/images/ftp/dir.png");
    } else {
        icon = getFileIconByType(name, type);
    }

    // 记录是否为目录
    listType[name] = type;
    listPath[name] = url.isDir();

    int row = ui->tw_ftpContent->rowCount();
    // 插入新的一行
    ui->tw_ftpContent->insertRow(row);
    // 名称
    ui->tw_ftpContent->setItem(row, 0, new QTableWidgetItem(icon, name));

    // 日期
    ui->tw_ftpContent->setItem(row, 1, new QTableWidgetItem(url.lastModified().toString("yyyy/MM/dd hh:mm")));

    // 类型
    ui->tw_ftpContent->setItem(row, 2, new QTableWidgetItem(type));

    // 大小
    if (url.isDir()) return;
    ui->tw_ftpContent->setItem(row, 3, new QTableWidgetItem(QString("%1 KB").arg(qMax(int(url.size() / 1000), 1))));
}

QIcon FtpDialog::getFileIconByType(QString fileName, QString &type) {
    QStringList sl = fileName.split(".");
    if (sl.size() == 1) {
        type = "";
        return QIcon(":/ftp/images/ftp/file.png");
    } else {
        type = sl.at(sl.size() - 1);
        switch (typeOptions.indexOf(type)) {
            case 0:
            case 1:
                return QIcon(":/ftp/images/ftp/zip.png");
                break;
            case 2:
                return QIcon(":/ftp/images/ftp/exe.png");
                break;
            default:
                return QIcon(":/ftp/images/ftp/file.png");
        }
    }
}

void FtpDialog::resizeEvent(QResizeEvent *event) {
    if (event) {
    }  //消除警告提示

    if (m_pMaskLayer != nullptr) {
        m_pMaskLayer->setAutoFillBackground(true);  //这个很重要，否则不会显示遮罩层
        QPalette pal = m_pMaskLayer->palette();
        pal.setColor(QPalette::Background, QColor(0x00, 0x00, 0x00, 0x20));
        m_pMaskLayer->setPalette(pal);
        m_pMaskLayer->setFixedSize(this->size());
    }
}

//显示
void FtpDialog::showLoadingForm() {
    if (m_pMaskLayer != nullptr) {
        m_pMaskLayer->setVisible(true);
    }
}
//隐藏
void FtpDialog::hideLoadingForm() {
    if (m_pMaskLayer != nullptr) {
        m_pMaskLayer->setVisible(false);
    }
}

void FtpDialog::on_tw_ftpContent_doubleClicked(const QModelIndex &index) {
    int     row  = index.row();
    QString name = ui->tw_ftpContent->item(row, 0)->text();

    // 如果双击的是第0行，并且不是根目录，因为根目录没有返回上一级项，表示返回上一级
    if (row == 0 && currentPath.indexOf("/") >= 0) {
        // 将当前目录减少一级
        currentPath = currentPath.left(currentPath.lastIndexOf("/"));

        // 清除表格
        clear();

        // 如果当前目录不是根目录，则先插入一行用来双击返回上一级
        if (currentPath.indexOf("/") >= 0) {
            ui->tw_ftpContent->insertRow(0);
            ui->tw_ftpContent->setItem(0, 0, new QTableWidgetItem("..."));
            ui->tw_ftpContent->setSpan(0, 0, 1, 4);
            listType["..."] = "None";
        }

        // 发送命令返回上一级，然后列出所有项
        ftp.cd("../");
        ftp.list();
    }
    // 如果双击的是其他行，并且是目录行，表示进入下一级
    else if (listPath[name]) {
        // 当前目录进入下一级
        currentPath += QString("/%1").arg(name);
        qDebug() << currentPath;
        // 清除表格
        clear();

        // 如果当前目录不是根目录，则先插入一行用来双击返回上一级
        ui->tw_ftpContent->insertRow(0);
        ui->tw_ftpContent->setItem(0, 0, new QTableWidgetItem("..."));
        ui->tw_ftpContent->setSpan(0, 0, 1, 4);
        listType["..."] = "None";

        // 发送命令进入下一级，然后列出所有项
        ftp.cd(ToSpecialEncoding(currentPath));
        //        ftp.cd(currentPath);
        ftp.list();
    }
}

void FtpDialog::clear() {
    listPath.clear();
    listType.clear();

    int rowCount = ui->tw_ftpContent->rowCount();
    for (int i = 0; i < rowCount; i++) {
        ui->tw_ftpContent->removeRow(0);
    }
}

void FtpDialog::contextMenuEvent(QContextMenuEvent *event) {
    Q_UNUSED(event);
    if (ui->tw_ftpContent->rowCount() <= 0) return;

    // 如果有未关闭的编辑项则先关闭
    //    closePersistentEditor();

    QMenu    menu;
    QAction *downA    = new QAction(QString::fromLocal8Bit("下载"), this);
    QAction *refreshA = new QAction(QString::fromLocal8Bit("刷新"), this);
    connect(downA, SIGNAL(triggered(bool)), this, SLOT(onDownload()));
    connect(refreshA, SIGNAL(triggered(bool)), this, SLOT(onRefresh()));
    menu.addAction(downA);
    menu.addAction(refreshA);

    menu.exec(QCursor::pos());
}

void FtpDialog::onRefresh() {
    // 清除表格
    clear();

    // 如果当前目录不是根目录，则先插入一行用来双击返回上一级
    if (currentPath.indexOf("/") >= 0) {
        ui->tw_ftpContent->insertRow(0);
        ui->tw_ftpContent->setItem(0, 0, new QTableWidgetItem("..."));
        ui->tw_ftpContent->setSpan(0, 0, 4, 4);
        listType["..."] = "None";
    }

    ftp.list();
}

void FtpDialog::onDownload() {
    int row = ui->tw_ftpContent->currentRow();
    if (row < 0) return;

    QString name = ui->tw_ftpContent->item(row, 0)->text();
    if (listPath[name]) {
        pendingDirs.append(currentPath + "/" + name);
    } else {
        QString path
            = QFileDialog::getSaveFileName(NULL, QString::fromLocal8Bit("选择下载位置"), QString("%1").arg(name));
        if (path.isEmpty()) return;
        file.setFileName(path);
        if (!file.open(QIODevice::WriteOnly)) return;

        // 解决中文乱码问题
        path = QString("%1/%2").arg(currentPath).arg(name);
        ftp.get(ToSpecialEncoding(path), &file);
    }
}

void FtpDialog::dataTransferProgress(qint64 readBytes, qint64 totalBytes) {
    ui->progressBar->setVisible(readBytes != totalBytes);
    ui->progressBar->setMaximum(totalBytes);
    ui->progressBar->setValue(readBytes);
}

//将gbk编码的字符串改为UTF-8编码，在获取FTP服务器下文件时使用
QString FtpDialog::FromSpecialEncoding(const QString &InputStr) {
#ifdef Q_OS_WIN
    return QString::fromLocal8Bit(InputStr.toLatin1());
#else
    QTextCodec *codec = QTextCodec::codecForName("gbk");
    if (codec) {
        return codec->toUnicode(InputStr.toLatin1());
    } else {
        return QString("");
    }
#endif
}

//将UTF-8编码的字符串改为gbk编码，在由客户端上传或下载文件时使用
QString FtpDialog::ToSpecialEncoding(const QString &InputStr) {
#ifdef Q_OS_WIN
    return QString::fromLatin1(InputStr.toLocal8Bit());
#else
    QTextCodec *codec = QTextCodec::codecForName("gbk");
    if (codec) {
        return QString::fromLatin1(codec->fromUnicode(InputStr));
    } else {
        return QString("");
    }
#endif
}

void FtpDialog::on_btn_disconnect_clicked() {}
