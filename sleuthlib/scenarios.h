#ifndef SCENARIOS_H
#define SCENARIOS_H
#include <QList>
#include <QMap>
#include <QString>

#include "scenarioStruct.h"
////
/// \brief The Scenarios class
/// a class to store datas in scenario file
class Scenarios {
public:
    static Scenarios *getInstance() {
        static Scenarios locla_s;
        return &locla_s;
    }

    typedef void (Scenarios::*Ptrtype)(QString);
    /**
     * @brief clearAllList
     * 清空所有List，方便重新赋值
     */
    void clearAllList();

    void    setInputDir(QString tmp);
    QString getInputDir();
    void    setOutputDir(QString tmp);
    QString getOutputDir();
    void    setWhirlgifBinary(QString tmp);
    QString getWhirlgifBinary();
    // ECHO
    void setECHO(bool tmp);
    bool getECHO();
    // WRITE_COEFF_FILE
    void setWCF(bool tmp);
    bool getWCF();
    // WRITE_AVG_FILE
    void setWAF(bool tmp);
    bool getWAF();
    // WRITE_STD_DEV_FILE
    void setWSDF(bool tmp);
    bool getWSDF();
    // WRITE_MEMORY_MAP
    void setWMM(bool tmp);
    bool getWMM();
    // LOGGING
    void setLOGGING(bool tmp);
    bool getLOGGING();
    //// ======================== ///

    // LOG_LANDCLASS_SUMMARY
    void setLLS(bool tmp);
    bool getLLS();
    // LOG_SLOPE_WEIGHTS
    void setLSW(bool tmp);
    bool getLSW();
    // LOG_READS
    void setLR(bool tmp);
    bool getLR();
    // LOG_WRITES
    void setLW(bool tmp);
    bool getLW();
    // LOG_COLORTABLES
    void setLC(bool tmp);
    bool getLC();
    // LOG_PROCESSING_STATUS
    void         setLPS(enum LOGENUM tmp);
    enum LOGENUM getLPS();
    // LOG_TIMINGS
    void         setLT(enum LOGENUM tmp);
    enum LOGENUM getLT();
    // LOG_TRANSITION_MATRIX
    void setLTM(bool tmp);
    bool getLTM();
    // LOG_URBANIZATION_ATTEMPTS
    void setLUA(bool tmp);
    bool getLUA();
    // LOG_INITIAL_COEFFICIENTS
    void setLIC(bool tmp);
    bool getLIC();
    // LOG_BASE_STATISTICS
    void setLBS(bool tmp);
    bool getLBS();
    // LOG_DEBUG
    void setLOGD(bool tmp);
    bool getLOGD();
    //// ======================== ///

    // # V. WORKING GRIDS
    void setNWG(int tmp);
    int  getNWG();
    // # VI. RANDOM NUMBER SEED
    void setRandSeed(int tmp);
    int  getRandSeed();
    // # VII. MONTE CARLO ITERATIONS
    void setMCI(int tmp);
    int  getMCI();

    /////// # VIII. COEFFICIENTS ////////
    Coefficients getCoefficients();
    void         setCDSTA(int tmp);
    int          getCDSTA();
    void         setCDSTE(int tmp);
    int          getCDSTE();
    void         setCDSTO(int tmp);
    int          getCDSTO();

    void setCBSTA(int tmp);
    int  getCBSTA();
    void setCBSTE(int tmp);
    int  getCBSTE();
    void setCBSTO(int tmp);
    int  getCBSTO();

    void setCSSTA(int tmp);
    int  getCSSTA();
    void setCSSTE(int tmp);
    int  getCSSTE();
    void setCSSTO(int tmp);
    int  getCSSTO();

    void setCSLSTA(int tmp);
    int  getCSLSTA();
    void setCSLSTE(int tmp);
    int  getCSLSTE();
    void setCSLSTO(int tmp);
    int  getCSLSTO();

    void setCRSTA(int tmp);
    int  getCRSTA();
    void setCRSTE(int tmp);
    int  getCRSTE();
    void setCRSTO(int tmp);
    int  getCRSTO();

    void setPDBF(int tmp);
    int  getPDBF();
    void setPBBF(int tmp);
    int  getPBBF();
    void setPSBF(int tmp);
    int  getPSBF();
    void setPSLBF(int tmp);
    int  getPSLBF();
    void setPRBF(int tmp);
    int  getPRBF();
    //// ======================== ///

    //////// IX. PREDICTION DATE RANGE /////
    PredictionDateRange getPredictionDateRange();
    void                setPSTAD(int tmp);
    int                 getPSTAD();
    void                setPSTOD(int tmp);
    int                 getPSTOD();
    //// ======================== ///

    /////  X. INPUT IMAGES ///////
    void        setUD(QString tmp);
    QString     getUD(int index = 0);
    QStringList getUDS();

    void        setRD(QString tmp);
    QString     getRD(int index = 0);
    QStringList getRDS();

    void        setLD(QString tmp);
    QString     getLD(int index = 0);
    QStringList getLDS();

    void    setED(QString tmp);
    QString getED();

    void    setSD(QString tmp);
    QString getSD();

    void    setBD(QString tmp);
    QString getBD();

    //// ======================== ///

    //// XI. OUTPUT IMAGES ////
    // WRITE_COLOR_KEY_IMAGES
    void setWCKI(bool tmp);
    bool getWCKI();
    // ECHO_IMAGE_FILES
    void setEIF(bool tmp);
    bool getEIF();
    // ANIMATION
    void setANIMATION(bool tmp);
    bool getANIMATION();

    //// ======================== ///

    //// # XII. COLORTABLE SETTINGS ////

    void   setDateColor(QColor c);
    QColor getDateColor();

    void   setSeedColor(QColor c);
    QColor getSeedColor();

    void   setWaterColor(QColor c);
    QColor getWaterColor();

    // 3. PROBABILITY COLORTABLE FOR URBAN GROWTH
    void                   setProbablityColor(ProbablityColor tmp);
    QColor                 getProbablityColorByRange(double probs);
    QColor                 getProbablityColorByIndex(int index);
    QList<ProbablityColor> getProbablityColors();

    // C. LAND COVER COLORTABLE
    void             setLandCColor(LandClass tmp);
    QColor           getLandCColorByPix(int pix);
    QColor           getLandCColorByIndex(int index);
    QList<LandClass> getLandCColors();

    // D. GROWTH TYPE IMAGE OUTPUT CONTROL AND COLORTABLE
    // VIEW_GROWTH_TYPES
    void setVGT(bool tmp);
    bool getVGT();
    // GROWTH_TYPE_PRINT_WINDOW
    void        setGTPW(PrintWindow tmp);
    PrintWindow getGTPW();
    // PHASE0G_GROWTH_COLOR
    void          setPGC(QColor tmp);
    QList<QColor> getPGCs();
    QColor        getPGCByIndex(int index);

    //  E. DELTATRON AGING SECTION
    // VIEW_DELTATRON_AGING
    void setVDA(bool tmp);
    bool getVDA();
    // DELTATRON_PRINT_WINDOW
    void        setDPW(PrintWindow tmp);
    PrintWindow getDPW();
    // DELTATRON_COLOR
    void                  setDC(DeltatronColor tmp);
    QList<DeltatronColor> getDCs();
    QColor                getDCByIndex(int index);

    ///// XIII. SELF-MODIFICATION PARAMETERS  ////
    SelfModfParams getSelfModificationParams();
    // ROAD_GRAV_SENSITIVITY
    void   setRGS(double tmp);
    double getRGS();
    // SLOPE_SENSITIVITY
    void   setSS(double tmp);
    double getSS();
    // CRITICAL_LOW
    void   setCL(double tmp);
    double getCL();
    // CRITICAL_HIGH
    void   setCH(double tmp);
    double getCH();
    // CRITICAL_SLOPE
    void   setCS(double tmp);
    double getCS();
    // BOOM
    void   setBOOM(double tmp);
    double getBOOM();
    // BUST
    void   setBUST(double tmp);
    double getBUST();
    //// ========================= ////////////
    void setIsInited(bool tmp);
    bool getIsInited();

private:
    ////// function //////
    Scenarios();
    ~Scenarios();

    /////  attribute //////
    bool              isInited;
    static Scenarios *local_instance;
    // 函数指针，用于优化大量if-else结构
    Ptrtype                _pmf;
    QMap<QString, Ptrtype> func_map;

    //// 配置文件变量 ////
    QString                input_dir;
    QString                output_dir;
    QString                whirlgif_binary;
    LogsAndOutputsControl  logControls;
    int                    numWorkingGrids;
    int                    randSeed;
    int                    monteCarloIterations;
    Coefficients           coefficients;
    PredictionDateRange    predictionDateRange;
    InputImageFiles        inputImageFiles;
    QColor                 DateColor;
    QColor                 SeedColor;
    QColor                 WaterColor;
    QList<ProbablityColor> probablity_colors;
    QList<LandClass>       landClass;
    PhaseGrowthColor       phaseGrowthColor;
    QList<DeltatronColor>  deltatronColors;
    SelfModfParams         smp;
};

#endif  // SCENARIOS_H
