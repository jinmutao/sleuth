﻿#include "loadingform.h"

#include "ui_loadingform.h"

LoadingForm::LoadingForm(QWidget *parent) : QWidget(parent), ui(new Ui::LoadingForm) {
    ui->setupUi(this);

    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);

    QMovie *movie;
    movie = new QMovie(":/ftp/images/ftp/load.gif");
    ui->label->setMovie(movie);
    movie->start();

    ui->label->setStyleSheet("background-color: transparent;");
}

LoadingForm::~LoadingForm() { delete ui; }
