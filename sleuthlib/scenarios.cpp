#include "scenarios.h"
/**
 * @brief Scenarios::Scenarios
 * 保存scenario配置文件中的相关信息
 * 采用单例模式
 */
Scenarios::Scenarios() { isInited = false; }

Scenarios::~Scenarios() {}

void Scenarios::clearAllList() {
    inputImageFiles.ROAD_DATAS.clear();
    inputImageFiles.URBAN_DATAS.clear();
    inputImageFiles.LANDUSE_DATA.clear();

    probablity_colors.clear();
    landClass.clear();
    phaseGrowthColor.pColors.clear();
    deltatronColors.clear();
}

void Scenarios::setInputDir(QString tmp) { input_dir = tmp; }

QString Scenarios::getInputDir() { return input_dir; }

void Scenarios::setOutputDir(QString tmp) { output_dir = tmp; }

QString Scenarios::getOutputDir() { return output_dir; }

void Scenarios::setWhirlgifBinary(QString tmp) { whirlgif_binary = tmp; }

QString Scenarios::getWhirlgifBinary() { return whirlgif_binary; }

void Scenarios::setECHO(bool tmp) { logControls.ECHO = tmp; }

bool Scenarios::getECHO() { return logControls.ECHO; }

void Scenarios::setWCF(bool tmp) { logControls.WRITE_COEFF_FILE = tmp; }

bool Scenarios::getWCF() { return logControls.WRITE_COEFF_FILE; }

void Scenarios::setWAF(bool tmp) { logControls.WRITE_AVG_FILE = tmp; }

bool Scenarios::getWAF() { return logControls.WRITE_AVG_FILE; }

void Scenarios::setWSDF(bool tmp) { logControls.WRITE_STD_DEV_FILE = tmp; }

bool Scenarios::getWSDF() { return logControls.WRITE_STD_DEV_FILE; }

void Scenarios::setWMM(bool tmp) { logControls.WRITE_MEMORY_MAP = tmp; }

bool Scenarios::getWMM() { return logControls.WRITE_MEMORY_MAP; }

void Scenarios::setLOGGING(bool tmp) { logControls.LOGGING = tmp; }

bool Scenarios::getLOGGING() { return logControls.LOGGING; }

void Scenarios::setLLS(bool tmp) { logControls.LOG_LANDCLASS_SUMMARY = tmp; }

bool Scenarios::getLLS() { return logControls.LOG_LANDCLASS_SUMMARY; }

void Scenarios::setLSW(bool tmp) { logControls.LOG_SLOPE_WEIGHTS = tmp; }

bool Scenarios::getLSW() { return logControls.LOG_SLOPE_WEIGHTS; }

void Scenarios::setLR(bool tmp) { logControls.LOG_READS = tmp; }

bool Scenarios::getLR() { return logControls.LOG_READS; }

void Scenarios::setLW(bool tmp) { logControls.LOG_WRITES = tmp; }

bool Scenarios::getLW() { return logControls.LOG_WRITES; }

void Scenarios::setLC(bool tmp) { logControls.LOG_COLORTABLES = tmp; }

bool Scenarios::getLC() { return logControls.LOG_COLORTABLES; }

void Scenarios::setLPS(enum LOGENUM tmp) { logControls.LOG_PROCESSING_STATUS = tmp; }

enum LOGENUM Scenarios::getLPS()
{
    return logControls.LOG_PROCESSING_STATUS;
}

void Scenarios::setLT(LOGENUM tmp) { logControls.LOG_TIMINGS = tmp; }

LOGENUM Scenarios::getLT() { return logControls.LOG_TIMINGS; }

void Scenarios::setLTM(bool tmp) { logControls.LOG_TRANSITION_MATRIX = tmp; }

bool Scenarios::getLTM() { return logControls.LOG_TRANSITION_MATRIX; }

void Scenarios::setLUA(bool tmp) { logControls.LOG_URBANIZATION_ATTEMPTS = tmp; }

bool Scenarios::getLUA() { return logControls.LOG_URBANIZATION_ATTEMPTS; }

void Scenarios::setLIC(bool tmp) { logControls.LOG_INITIAL_COEFFICIENTS = tmp; }

bool Scenarios::getLIC() { return logControls.LOG_INITIAL_COEFFICIENTS; }

void Scenarios::setLBS(bool tmp) { logControls.LOG_BASE_STATISTICS = tmp; }

bool Scenarios::getLBS() { return logControls.LOG_BASE_STATISTICS; }

void Scenarios::setLOGD(bool tmp) { logControls.LOG_DEBUG = tmp; }

bool Scenarios::getLOGD() { return logControls.LOG_DEBUG; }

void Scenarios::setNWG(int tmp) { numWorkingGrids = tmp; }

int Scenarios::getNWG() { return numWorkingGrids; }

void Scenarios::setRandSeed(int tmp) { randSeed = tmp; }

int Scenarios::getRandSeed() { return randSeed; }

void Scenarios::setMCI(int tmp) { monteCarloIterations = tmp; }

int Scenarios::getMCI() { return monteCarloIterations; }

Coefficients Scenarios::getCoefficients() { return coefficients; }

void Scenarios::setCDSTA(int tmp) { coefficients.diffusion.start = tmp; }

int Scenarios::getCDSTA() { return coefficients.diffusion.start; }

void Scenarios::setCDSTE(int tmp) { coefficients.diffusion.step = tmp; }

int Scenarios::getCDSTE() { return coefficients.diffusion.step; }

void Scenarios::setCDSTO(int tmp) { coefficients.diffusion.stop = tmp; }

int Scenarios::getCDSTO() { return coefficients.diffusion.stop; }

void Scenarios::setCBSTA(int tmp) { coefficients.breed.start = tmp; }

int Scenarios::getCBSTA() { return coefficients.breed.start; }

void Scenarios::setCBSTE(int tmp) { coefficients.breed.step = tmp; }

int Scenarios::getCBSTE() { return coefficients.breed.step; }

void Scenarios::setCBSTO(int tmp) { coefficients.breed.stop = tmp; }

int Scenarios::getCBSTO() { return coefficients.breed.stop; }

void Scenarios::setCSSTA(int tmp) { coefficients.spread.start = tmp; }

int Scenarios::getCSSTA() { return coefficients.spread.start; }

void Scenarios::setCSSTE(int tmp) { coefficients.spread.step = tmp; }

int Scenarios::getCSSTE() { return coefficients.spread.step; }

void Scenarios::setCSSTO(int tmp) { coefficients.spread.stop = tmp; }

int Scenarios::getCSSTO() { return coefficients.spread.stop; }

void Scenarios::setCSLSTA(int tmp) { coefficients.slope.start = tmp; }

int Scenarios::getCSLSTA() { return coefficients.slope.start; }

void Scenarios::setCSLSTE(int tmp) { coefficients.slope.step = tmp; }

int Scenarios::getCSLSTE() { return coefficients.slope.step; }

void Scenarios::setCSLSTO(int tmp) { coefficients.slope.stop = tmp; }

int Scenarios::getCSLSTO() { return coefficients.slope.stop; }

void Scenarios::setCRSTA(int tmp) { coefficients.road.start = tmp; }

int Scenarios::getCRSTA() { return coefficients.road.start; }

void Scenarios::setCRSTE(int tmp) { coefficients.road.step = tmp; }

int Scenarios::getCRSTE() { return coefficients.road.step; }

void Scenarios::setCRSTO(int tmp) { coefficients.road.stop = tmp; }

int Scenarios::getCRSTO() { return coefficients.road.stop; }

void Scenarios::setPDBF(int tmp) { coefficients.bestFit.diffusion = tmp; }

int Scenarios::getPDBF() { return coefficients.bestFit.diffusion; }

void Scenarios::setPBBF(int tmp) { coefficients.bestFit.breed = tmp; }

int Scenarios::getPBBF() { return coefficients.bestFit.breed; }

void Scenarios::setPSBF(int tmp) { coefficients.bestFit.spread = tmp; }

int Scenarios::getPSBF() { return coefficients.bestFit.spread; }

void Scenarios::setPSLBF(int tmp) { coefficients.bestFit.slope = tmp; }

int Scenarios::getPSLBF() { return coefficients.bestFit.slope; }

void Scenarios::setPRBF(int tmp) { coefficients.bestFit.road = tmp; }

int Scenarios::getPRBF() { return coefficients.bestFit.road; }

PredictionDateRange Scenarios::getPredictionDateRange() { return predictionDateRange; }

void Scenarios::setPSTAD(int tmp) { predictionDateRange.PREDICTION_START_DATE = tmp; }

int Scenarios::getPSTAD() { return predictionDateRange.PREDICTION_START_DATE; }

void Scenarios::setPSTOD(int tmp) { predictionDateRange.PREDICTION_STOP_DATE = tmp; }

int Scenarios::getPSTOD() { return predictionDateRange.PREDICTION_STOP_DATE; }

void Scenarios::setUD(QString tmp) { inputImageFiles.URBAN_DATAS.append(tmp); }

QString Scenarios::getUD(int index) {
    index = index < inputImageFiles.URBAN_DATAS.size() ? index : 0;
    return inputImageFiles.URBAN_DATAS[index];
}

QStringList Scenarios::getUDS() { return inputImageFiles.URBAN_DATAS; }

void Scenarios::setRD(QString tmp) { inputImageFiles.ROAD_DATAS.append(tmp); }

QString Scenarios::getRD(int index) { return inputImageFiles.ROAD_DATAS[index]; }

QStringList Scenarios::getRDS() { return inputImageFiles.ROAD_DATAS; }

void Scenarios::setLD(QString tmp) { inputImageFiles.LANDUSE_DATA.append(tmp); }

QString Scenarios::getLD(int index) { return inputImageFiles.LANDUSE_DATA[index]; }

QStringList Scenarios::getLDS() { return inputImageFiles.LANDUSE_DATA; }

void Scenarios::setED(QString tmp) { inputImageFiles.EXCLUDED_DATA = tmp; }

QString Scenarios::getED() { return inputImageFiles.EXCLUDED_DATA; }

void Scenarios::setSD(QString tmp) { inputImageFiles.SLOPE_DATA = tmp; }

QString Scenarios::getSD() { return inputImageFiles.SLOPE_DATA; }

void Scenarios::setBD(QString tmp) { inputImageFiles.BACKGROUND_DATA = tmp; }

QString Scenarios::getBD() { return inputImageFiles.BACKGROUND_DATA; }

void Scenarios::setWCKI(bool tmp) { logControls.WRITE_COLOR_KEY_IMAGES = tmp; }

bool Scenarios::getWCKI() { return logControls.WRITE_COLOR_KEY_IMAGES; }

void Scenarios::setEIF(bool tmp) { logControls.ECHO_IMAGE_FILES = tmp; }

bool Scenarios::getEIF() { return logControls.ECHO_IMAGE_FILES; }

void Scenarios::setANIMATION(bool tmp) { logControls.ANIMATION = tmp; }

bool Scenarios::getANIMATION() { return logControls.ANIMATION; }

void Scenarios::setDateColor(QColor c) { DateColor = c; }

QColor Scenarios::getDateColor() { return DateColor; }

void Scenarios::setSeedColor(QColor c) { SeedColor = c; }

QColor Scenarios::getSeedColor() { return SeedColor; }

void Scenarios::setWaterColor(QColor c) { WaterColor = c; }

QColor Scenarios::getWaterColor() { return WaterColor; }

void Scenarios::setProbablityColor(ProbablityColor tmp) { probablity_colors.append(tmp); }

QColor Scenarios::getProbablityColorByRange(double probs) {
    for (ProbablityColor c : probablity_colors) {
        if (c.low <= probs && probs < c.high) {
            return c.color;
        }
    }
    return QColor(200, 200, 200);
}

QColor Scenarios::getProbablityColorByIndex(int index) { return probablity_colors[index].color; }

QList<ProbablityColor> Scenarios::getProbablityColors() { return probablity_colors; }

void Scenarios::setLandCColor(LandClass tmp) { landClass.append(tmp); }

QColor Scenarios::getLandCColorByPix(int pix) {
    for (LandClass c : landClass) {
        if (c.pix == pix) {
            return c.color;
        }
    }
    return QColor(200, 200, 200);
}

QColor Scenarios::getLandCColorByIndex(int index) { return landClass[index].color; }

QList<LandClass> Scenarios::getLandCColors() { return landClass; }

void Scenarios::setVGT(bool tmp) { logControls.VIEW_GROWTH_TYPES = tmp; }

bool Scenarios::getVGT() { return logControls.VIEW_GROWTH_TYPES; }

void Scenarios::setGTPW(PrintWindow tmp) { logControls.GROWTH_TYPE_PRINT_WINDOW = tmp; }

PrintWindow Scenarios::getGTPW() { return logControls.GROWTH_TYPE_PRINT_WINDOW; }

void Scenarios::setPGC(QColor tmp) { phaseGrowthColor.pColors.append(tmp); }

QList<QColor> Scenarios::getPGCs() { return phaseGrowthColor.pColors; }

QColor Scenarios::getPGCByIndex(int index) { return phaseGrowthColor.pColors[index]; }

void Scenarios::setVDA(bool tmp) { logControls.VIEW_DELTATRON_AGING = tmp; }

bool Scenarios::getVDA() { return logControls.VIEW_DELTATRON_AGING; }

void Scenarios::setDPW(PrintWindow tmp) { logControls.DELTATRON_PRINT_WINDOW = tmp; }

PrintWindow Scenarios::getDPW() { return logControls.DELTATRON_PRINT_WINDOW; }

void Scenarios::setDC(DeltatronColor tmp) { deltatronColors.append(tmp); }

QList<DeltatronColor> Scenarios::getDCs() { return deltatronColors; }

QColor Scenarios::getDCByIndex(int index) { return deltatronColors[index].dColors; }

SelfModfParams Scenarios::getSelfModificationParams() { return smp; }

void Scenarios::setRGS(double tmp) { smp.ROAD_GRAV_SENSITIVITY = tmp; }

double Scenarios::getRGS() { return smp.ROAD_GRAV_SENSITIVITY; }

void Scenarios::setSS(double tmp) { smp.SLOPE_SENSITIVITY = tmp; }

double Scenarios::getSS() { return smp.SLOPE_SENSITIVITY; }

void Scenarios::setCL(double tmp) { smp.CRITICAL_LOW = tmp; }

double Scenarios::getCL() { return smp.CRITICAL_LOW; }

void Scenarios::setCH(double tmp) { smp.CRITICAL_HIGH = tmp; }

double Scenarios::getCH() { return smp.CRITICAL_HIGH; }

void Scenarios::setCS(double tmp) { smp.CRITICAL_SLOPE = tmp; }

double Scenarios::getCS() { return smp.CRITICAL_SLOPE; }

void Scenarios::setBOOM(double tmp) { smp.BOOM = tmp; }

double Scenarios::getBOOM() { return smp.BOOM; }

void Scenarios::setBUST(double tmp) { smp.BUST = tmp; }

double Scenarios::getBUST() { return smp.BUST; }

void Scenarios::setIsInited(bool tmp) { isInited = tmp; }

bool Scenarios::getIsInited() { return isInited; }
