#include "utils.h"

QString MYUTILS::converRGB16HexStr(QColor _color, QString header) {
    // r的rgb转化为16进制
    QString redStr = QString("%1").arg(_color.red(), 2, 16, QChar('0'));
    // g的rgb转化为16进制
    QString greenStr = QString("%1").arg(_color.green(), 2, 16, QChar('0'));
    // b的rgb转化为16进制
    QString blueStr = QString("%1").arg(_color.blue(), 2, 16, QChar('0'));
    //将各rgb的拼接在一起形成#000000
    QString hexStr = header + redStr + greenStr + blueStr;
    //返回hexStr
    return hexStr;
}

QString MYUTILS::converRGBStr(QColor _color) {
    return QString("(%1, %2, %3)").arg(_color.red()).arg(_color.green()).arg(_color.blue());
}

QColor MYUTILS::convertHex2QC(QString hexStr) {
    QColor c(hexStr.toUInt(NULL, 16));
    return c;
}

QColor MYUTILS::converStr2QC(QString colorStr) {
    if (colorStr.indexOf(",") != -1) {
        QStringList splitList = colorStr.split(",");
        Q_ASSERT(splitList.size() >= 3);
        return QColor(splitList[0].toInt(), splitList[1].toInt(), splitList[2].toInt());
    } else {
        return MYUTILS::convertHex2QC(colorStr);
    }
}

QString MYUTILS::getRelativeDirPath(QString dirPath) {
    //    QStringList splitList = dirPath.split("/");
    //    int         len       = splitList.size();
    //    dirPath               = "./" + splitList[len - 2] + "/" + splitList[len - 1];
    dirPath += "/";
    return dirPath;
}
