﻿#ifndef SCENARIOFORM_H
#define SCENARIOFORM_H

/**
 * @projectName   msleuth
 * @brief         摘要
 * @author        cmk
 * @date          2020-10-15
 */

#include <QColorDialog>
#include <QSettings>
#include <QTableWidgetItem>
#include <QWidget>
#include <exception>

#include "sleuthlib/scenarioStruct.h"
#include "sleuthlib/scenarios.h"
#include "sleuthlib/utils.h"

namespace Ui {
class ScenarioForm;
}

class ScenarioForm : public QDialog {
    Q_OBJECT

public:
    explicit ScenarioForm(QWidget *parent = nullptr);
    ~ScenarioForm();
    /**
     * @brief loadData2Tab2
     * 加载数据到tab2中
     */
    void loadData2Tab2();

    /**
     * @brief setScenarioContents
     * @param contents 需要加载的内容，若为空，则从文件中读取
     * 设置tab1中textEdit的内容
     */
    void setScenarioContents(QString contents = nullptr);

    /**
     * @brief setScenarioPath
     * @param tmp 路径
     * 设置配置文件路径
     */
    void setScenarioPath(QString tmp);
    void setScenarioDetail(QStringList sd);

    /**
     * @brief initForm
     * 用于初始化当前界面的一些信息
     */
    void initForm(QString);

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    /**
     * @brief show_save_hint
     * 当用户切换tab时，提示是否保存
     */
    void show_save_hint();

    /**
     * @brief tab1_save
     * 当TabWidget在1中是，调用该方法保存Scenario中的内容
     */
    void tab1_save();

    void on_scenario_save_clicked();
    void btn_delete_clicked();
    void btn_delete_probItem_clicked();
    void btn_delete_landItem_clicked();
    void btn_delete_DItem_clicked();
    void colorCellClicked(QTableWidgetItem *item);
    void on_btn_addImage_clicked();

    void on_btn_addProbColor_clicked();

    void on_btn_addLandColor_clicked();

    void on_btn_addDColor_clicked();

    void on_btn_2sw1_clicked();

    void on_btn_2sw2_clicked();

    void on_btn_2sw3_clicked();

    void on_btn_2sw4_clicked();

    void on_btn_sinputDIr_clicked();

    void hasChanged();

    void on_btn_soutputDir_clicked();

    void on_btn_sWhiBin_clicked();

private:
    Ui::ScenarioForm *ui;
    QString           scenarioPath;
    QStringList *     imageTypes;
    QStringList *     flagTypes;
    bool              changed;
    Scenarios *       scen;
    void              initTable();
    void              initImageTable();
    void              initColorTable();
    void              deleteTableItem(QTableWidget *);
    void              addProbColorsItem(ProbablityColor);
    void              addLandColorsItem(LandClass);

    /**
     * @brief addImageInputItem
     * @param inImage 输入图像名称 如URBAN_DATA
     * @param cname 图像对应的类别 如demo200.urban.1930.gif
     * 添加一个输入图像
     */
    void addImageInputItem(QString inImage, QString cname);
    void connectSlots();

    /**
     * @brief updateScenario
     * 将窗口中的内容更新到scen对象中
     */
    void updateScenario();
};

#endif  // SCENARIOFORM_H
