#include "parse_file.h"

ParseFile::ParseFile() {
    //    func_map["test"] = &ParseFile::test;
    func_map["INPUT_DIR"]       = &ParseFile::parseInputDir;
    func_map["OUTPUT_DIR"]      = &ParseFile::parseOutputDir;
    func_map["WHIRLGIF_BINARY"] = &ParseFile::parseWhirlgifBin;
    func_map["ECHO(YES/NO)"]    = &ParseFile::parseECHO;

    // # III. Output Files
    func_map["WRITE_COEFF_FILE(YES/NO)"]   = &ParseFile::parseWCF;
    func_map["WRITE_AVG_FILE(YES/NO)"]     = &ParseFile::parseWAF;
    func_map["WRITE_STD_DEV_FILE(YES/NO)"] = &ParseFile::parseWSDF;
    func_map["WRITE_MEMORY_MAP(YES/NO)"]   = &ParseFile::parseWMM;
    func_map["LOGGING(YES/NO)"]            = &ParseFile::parseLOGGING;

    // # IV. Log File Preferences
    func_map["LOG_LANDCLASS_SUMMARY(YES/NO)"]                                 = &ParseFile::parseLLS;
    func_map["LOG_SLOPE_WEIGHTS(YES/NO)"]                                     = &ParseFile::parseLSW;
    func_map["LOG_READS(YES/NO)"]                                             = &ParseFile::parseLR;
    func_map["LOG_WRITES(YES/NO)"]                                            = &ParseFile::parseLW;
    func_map["LOG_COLORTABLES(YES/NO)"]                                       = &ParseFile::parseLC;
    func_map["LOG_PROCESSING_STATUS(0:off/1:low verbosity/2:high verbosity)"] = &ParseFile::parseLPS;
    func_map["LOG_TRANSITION_MATRIX(YES/NO)"]                                 = &ParseFile::parseLTM;
    func_map["LOG_URBANIZATION_ATTEMPTS(YES/NO)"]                             = &ParseFile::parseLUA;
    func_map["LOG_INITIAL_COEFFICIENTS(YES/NO)"]                              = &ParseFile::parseLIC;
    func_map["LOG_BASE_STATISTICS(YES/NO)"]                                   = &ParseFile::parseLBS;
    func_map["LOG_DEBUG(YES/NO)"]                                             = &ParseFile::parseLOGD;
    func_map["LOG_TIMINGS(0:off/1:low verbosity/2:high verbosity)"]           = &ParseFile::parseLT;

    // # V. WORKING GRIDS
    func_map["NUM_WORKING_GRIDS"]      = &ParseFile::parseNWG;
    func_map["RANDOM_SEED"]            = &ParseFile::parseRS;
    func_map["MONTE_CARLO_ITERATIONS"] = &ParseFile::parseMCI;

    // # VIII. COEFFICIENTS
    func_map["CALIBRATION_DIFFUSION_START"] = &ParseFile::parseCDSTA;
    func_map["CALIBRATION_DIFFUSION_STEP"]  = &ParseFile::parseCDSTE;
    func_map["CALIBRATION_DIFFUSION_STOP"]  = &ParseFile::parseCDSTO;

    func_map["CALIBRATION_BREED_START"] = &ParseFile::parseCBSTA;
    func_map["CALIBRATION_BREED_STEP"]  = &ParseFile::parseCBSTE;
    func_map["CALIBRATION_BREED_STOP"]  = &ParseFile::parseCBSTO;

    func_map["CALIBRATION_SPREAD_START"] = &ParseFile::parseCSSTA;
    func_map["CALIBRATION_SPREAD_STEP"]  = &ParseFile::parseCSSTE;
    func_map["CALIBRATION_SPREAD_STOP"]  = &ParseFile::parseCSSTO;

    func_map["CALIBRATION_SLOPE_START"] = &ParseFile::parseCSLSTA;
    func_map["CALIBRATION_SLOPE_STEP"]  = &ParseFile::parseCSLSTE;
    func_map["CALIBRATION_SLOPE_STOP"]  = &ParseFile::parseCSLSTO;

    func_map["CALIBRATION_ROAD_START"] = &ParseFile::parseCRSTA;
    func_map["CALIBRATION_ROAD_STEP"]  = &ParseFile::parseCRSTE;
    func_map["CALIBRATION_ROAD_STOP"]  = &ParseFile::parseCRSTO;

    func_map["PREDICTION_DIFFUSION_BEST_FIT"] = &ParseFile::parsePDBF;
    func_map["PREDICTION_BREED_BEST_FIT"]     = &ParseFile::parsePBBF;
    func_map["PREDICTION_SPREAD_BEST_FIT"]    = &ParseFile::parsePSBF;
    func_map["PREDICTION_SLOPE_BEST_FIT"]     = &ParseFile::parsePSLBF;
    func_map["PREDICTION_ROAD_BEST_FIT"]      = &ParseFile::parsePRBF;

    // # IX. PREDICTION DATE RANGE
    func_map["PREDICTION_START_DATE"] = &ParseFile::parsePSTAD;
    func_map["PREDICTION_STOP_DATE"]  = &ParseFile::parsePSTOD;

    // # X. INPUT IMAGES
    func_map["URBAN_DATA"]      = &ParseFile::parseUD;
    func_map["ROAD_DATA"]       = &ParseFile::parseRD;
    func_map["LANDUSE_DATA"]    = &ParseFile::parseLD;
    func_map["EXCLUDED_DATA"]   = &ParseFile::parseED;
    func_map["SLOPE_DATA"]      = &ParseFile::parseSD;
    func_map["BACKGROUND_DATA"] = &ParseFile::parseBD;

    // # XI. OUTPUT IMAGES
    func_map["WRITE_COLOR_KEY_IMAGES(YES/NO)"] = &ParseFile::parseWCKI;
    func_map["ECHO_IMAGE_FILES(YES/NO)"]       = &ParseFile::parseEIF;
    func_map["ANIMATION(YES/NO)"]              = &ParseFile::parseANIMATION;

    // # XII. COLORTABLE SETTINGS
    func_map["DATE_COLOR"]  = &ParseFile::parseDateColor;
    func_map["SEED_COLOR"]  = &ParseFile::parseSeedColor;
    func_map["WATER_COLOR"] = &ParseFile::parseWaterColor;

    func_map["PROBABILITY_COLOR"] = &ParseFile::parseProbablityColor;
    func_map["LANDUSE_CLASS"]     = &ParseFile::parseLandClassColor;

    // D. GROWTH TYPE IMAGE OUTPUT CONTROL AND COLORTABLE
    func_map["VIEW_GROWTH_TYPES(YES/NO)"] = &ParseFile::parseVGT;
    func_map["GROWTH_TYPE_PRINT_WINDOW"]  = &ParseFile::parseGPTW;
    func_map["PHASE0G_GROWTH_COLOR"]      = &ParseFile::parsePGC;
    func_map["PHASE1G_GROWTH_COLOR"]      = &ParseFile::parsePGC;
    func_map["PHASE2G_GROWTH_COLOR"]      = &ParseFile::parsePGC;
    func_map["PHASE3G_GROWTH_COLOR"]      = &ParseFile::parsePGC;
    func_map["PHASE4G_GROWTH_COLOR"]      = &ParseFile::parsePGC;
    func_map["PHASE5G_GROWTH_COLOR"]      = &ParseFile::parsePGC;

    // E. DELTATRON AGING SECTION
    func_map["VIEW_DELTATRON_AGING(YES/NO)"] = &ParseFile::parseVDA;
    func_map["DELTATRON_PRINT_WINDOW"]       = &ParseFile::parseDPW;
    func_map["DELTATRON_COLOR"]              = &ParseFile::parseDC;

    // XIII. SELF-MODIFICATION PARAMETERS
    func_map["ROAD_GRAV_SENSITIVITY"] = &ParseFile::parseRGS;
    func_map["SLOPE_SENSITIVITY"]     = &ParseFile::parseSS;
    func_map["CRITICAL_LOW"]          = &ParseFile::parseCL;
    func_map["CRITICAL_HIGH"]         = &ParseFile::parseCH;
    func_map["CRITICAL_SLOPE"]        = &ParseFile::parseCS;
    func_map["BOOM"]                  = &ParseFile::parseBOOM;
    func_map["BUST"]                  = &ParseFile::parseBUST;
}

ParseFile::~ParseFile() {}

Scenarios *ParseFile::file2Scenario(QString filePath) {
    QFile      file(filePath);
    Scenarios *scenarios = Scenarios::getInstance();
    try {
        file.open(QFile::ReadOnly | QFile::Text);
        // 清洗数据
        QString   lineStr;
        ParseFile pf;
        while (!file.atEnd()) {
            lineStr = file.readLine();
            pf.parseLine(lineStr, scenarios);
        }
        scenarios->setIsInited(true);
    } catch (std::exception) {
        scenarios = nullptr;
    }
    file.close();
    return scenarios;
}

bool ParseFile::Scenario2file(QString filePath) {
    Scenarios *scenarios = Scenarios::getInstance();
    // 是否成功写入
    bool      status = true;
    ParseFile pf;
    QString   splitStr = " \n\n# =======================================\n\n";
    QString   endStr   = " \n";
    QFile     file(filePath);
    file.open(QFile::WriteOnly | QFile::Text);
    try {
        QTextStream txtOutput(&file);
        txtOutput << MHINTS::PREVIEW_ALL_HINTS << "\n";

        txtOutput << MHINTS::I_PATH_NAME_VARIABLES_HINTS << "\n";
        txtOutput << QString("INPUT_DIR=%1").arg(scenarios->getInputDir()) << endStr;
        txtOutput << QString("OUTPUT_DIR=%1").arg(scenarios->getOutputDir()) << endStr;
        txtOutput << QString("WHIRLGIF_BINARY=%1").arg(scenarios->getWhirlgifBinary()) << splitStr;

        txtOutput << MHINTS::II_RUNNING_STATUS__HINTS << "\n";
        txtOutput << QString("ECHO(YES/NO)=%1").arg(bool2str(scenarios->getECHO())) << splitStr;

        txtOutput << MHINTS::III_Output_Files_HINTS << "\n";
        txtOutput << QString("WRITE_COEFF_FILE(YES/NO)=%1").arg(bool2str(scenarios->getWCF())) << endStr;
        txtOutput << QString("WRITE_AVG_FILE(YES/NO)=%1").arg(bool2str(scenarios->getWAF())) << endStr;
        txtOutput << QString("WRITE_STD_DEV_FILE(YES/NO)=%1").arg(bool2str(scenarios->getWSDF())) << endStr;
        txtOutput << QString("WRITE_MEMORY_MAP(YES/NO)=%1").arg(bool2str(scenarios->getWMM())) << endStr;
        txtOutput << QString("LOGGING(YES/NO)=%1").arg(bool2str(scenarios->getLOGGING())) << splitStr;

        txtOutput << MHINTS::IV_Log_File_Preferences_HINTS << "\n";
        txtOutput << QString("LOG_LANDCLASS_SUMMARY(YES/NO)=%1").arg(bool2str(scenarios->getLLS())) << endStr;
        txtOutput << QString("LOG_SLOPE_WEIGHTS(YES/NO)=%1").arg(bool2str(scenarios->getLSW())) << endStr;
        txtOutput << QString("LOG_READS(YES/NO)=%1").arg(bool2str(scenarios->getLR())) << endStr;
        txtOutput << QString("LOG_WRITES(YES/NO)=%1").arg(bool2str(scenarios->getLW())) << endStr;
        txtOutput << QString("LOG_COLORTABLES(YES/NO)=%1").arg(bool2str(scenarios->getLC())) << endStr;
        txtOutput
            << QString("LOG_PROCESSING_STATUS(0:off/1:low verbosity/2:high verbosity)=%1").arg((int)scenarios->getLPS())
            << endStr;
        txtOutput << QString("LOG_TRANSITION_MATRIX(YES/NO)=%1").arg(bool2str(scenarios->getLTM())) << endStr;
        txtOutput << QString("LOG_URBANIZATION_ATTEMPTS(YES/NO)=%1").arg(bool2str(scenarios->getLUA())) << endStr;
        txtOutput << QString("LOG_INITIAL_COEFFICIENTS(YES/NO)=%1").arg(bool2str(scenarios->getLIC())) << endStr;
        txtOutput << QString("LOG_DEBUG(YES/NO)=%1").arg(bool2str(scenarios->getLOGD())) << endStr;
        txtOutput << QString("LOG_TIMINGS(0:off/1:low verbosity/2:high verbosity)=%1").arg((int)scenarios->getLT())
                  << splitStr;

        txtOutput << MHINTS::V_WORKING_GRIDS_HINTS << "\n";
        txtOutput << QString("NUM_WORKING_GRIDS=%1").arg(scenarios->getNWG()) << splitStr;

        txtOutput << MHINTS::VI_RANDOM_NUMBER_SEED_HINTS << "\n";
        txtOutput << QString("RANDOM_SEED=%1").arg(scenarios->getRandSeed()) << splitStr;

        txtOutput << MHINTS::VII_MONTE_CARLO_ITERATIONS_HINTS << "\n";
        txtOutput << QString("MONTE_CARLO_ITERATIONS=%1").arg(scenarios->getMCI()) << splitStr;

        txtOutput << MHINTS::VIII_COEFFICIENTS_HINTS << "\n";
        Coefficients coeff = scenarios->getCoefficients();
        txtOutput << coeff.diffusion.toString() << endStr;
        txtOutput << coeff.breed.toString() << endStr;
        txtOutput << coeff.spread.toString() << endStr;
        txtOutput << coeff.slope.toString() << endStr;
        txtOutput << coeff.road.toString() << endStr;
        txtOutput << coeff.bestFit.toString() << splitStr;

        txtOutput << MHINTS::IX_PREDICTION_DATE_RANGE_HINTS << "\n";
        txtOutput << scenarios->getPredictionDateRange().toString() << splitStr;

        txtOutput << MHINTS::X_INPUT_IMAGES_HINTS << "\n";
        for (QString tmp : scenarios->getUDS()) {
            txtOutput << QString("URBAN_DATA=%1").arg(tmp) << endStr;
        }
        txtOutput << endStr;
        for (QString tmp : scenarios->getRDS()) {
            txtOutput << QString("ROAD_DATA=%1").arg(tmp) << endStr;
        }
        txtOutput << endStr;
        for (QString tmp : scenarios->getLDS()) {
            txtOutput << QString("LANDUSE_DATA=%1").arg(tmp) << endStr;
        }
        txtOutput << endStr << QString("EXCLUDED_DATA=%1").arg(scenarios->getED()) << endStr;
        txtOutput << endStr << QString("SLOPE_DATA=%1").arg(scenarios->getSD()) << endStr;
        txtOutput << endStr << QString("BACKGROUND_DATA=%1").arg(scenarios->getBD()) << splitStr;

        txtOutput << MHINTS::XI_OUTPUT_IMAGES_HINTS << endStr;
        txtOutput << QString("WRITE_COLOR_KEY_IMAGES(YES/NO)=%1").arg(bool2str(scenarios->getWCKI())) << endStr;
        txtOutput << QString("ECHO_IMAGE_FILES(YES/NO)=%1").arg(bool2str(scenarios->getEIF())) << endStr;
        txtOutput << QString("ANIMATION(YES/NO)=%1").arg(bool2str(scenarios->getANIMATION())) << endStr;

        txtOutput << MHINTS::XII_COLORTABLE_SETTINGS_HINTS << endStr;
        txtOutput << QString("DATE_COLOR=\t%1").arg(MYUTILS::converRGB16HexStr(scenarios->getDateColor())) << endStr;
        txtOutput << QString("SEED_COLOR=\t%1").arg(MYUTILS::converRGB16HexStr(scenarios->getSeedColor())) << endStr;
        txtOutput << QString("WATER_COLOR=\t%1").arg(MYUTILS::converRGB16HexStr(scenarios->getWaterColor())) << endStr
                  << endStr;

        txtOutput << MHINTS::XII3_PROBABILITY_COLOR_HINTS << endStr;
        for (ProbablityColor tmp : scenarios->getProbablityColors()) {
            txtOutput << tmp.toString() << endStr;
        }

        txtOutput << MHINTS::XIIC_LANDCOLOR_HINTS << endStr;
        for (LandClass tmp : scenarios->getLandCColors()) {
            txtOutput << tmp.toString() << endStr;
        }
        txtOutput << splitStr << endStr;

        txtOutput << MHINTS::XIID_GROWTHCOLOR_HINTS << endStr;
        txtOutput << QString("VIEW_GROWTH_TYPES(YES/NO)=%1").arg(bool2str(scenarios->getVGT())) << endStr;
        txtOutput << QString("GROWTH_TYPE_PRINT_WINDOW=%1").arg(scenarios->getGTPW().toString()) << endStr;
        for (int i = 0; i < scenarios->getPGCs().size(); i++) {
            txtOutput << QString("PHASE%1G_GROWTH_COLOR=    %2")
                             .arg(i)
                             .arg(MYUTILS::converRGB16HexStr(scenarios->getPGCByIndex(i)))
                      << endStr;
        }
        txtOutput << splitStr << endStr;

        txtOutput << MHINTS::XIIE_DELTATRONCOLOR_HINTS << endStr;
        txtOutput << QString("VIEW_DELTATRON_AGING(YES/NO)=%1").arg(bool2str(scenarios->getVDA())) << endStr;
        txtOutput << QString("DELTATRON_PRINT_WINDOW=%1").arg(scenarios->getDPW().toString()) << endStr;
        for (int i = 0; i < scenarios->getDCs().size(); i++) {
            txtOutput << QString("DELTATRON_COLOR=    %1").arg(MYUTILS::converRGB16HexStr(scenarios->getDCByIndex(i)))
                      << endStr;
        }
        txtOutput << splitStr << endStr;

        txtOutput << MHINTS::XIIC_LANDCOLOR_HINTS << endStr;
        txtOutput << scenarios->getSelfModificationParams().toString() << endStr;
    } catch (std::exception) {
        status = false;
    }

    file.close();
    return status;
}

void ParseFile::parseLine(QString lineStr, Scenarios *scenarios) {
    QString     key;
    QStringList splitList;
    // 去掉#注释的内容和换行符
    if (lineStr[0] != "#" && lineStr[0] != "\xa") {
        lineStr.replace("\xa", "");
        splitList = lineStr.split("#")[0].split("=");
        if (splitList.size() < 2) {
            return;
        }
        key = splitList[0].trimmed();
        if (key.size() > 0 && this->func_map.contains(key)) {
            this->_pmf = this->func_map[key];  //取出对应的函数指针
            (this->*_pmf)(splitList[1], scenarios);
        }
    }
}

void ParseFile::parseInputDir(QString tmp, Scenarios *scenario) { scenario->setInputDir(tmp.trimmed()); }

void ParseFile::parseOutputDir(QString tmp, Scenarios *scenario) { scenario->setOutputDir(tmp.trimmed()); }

void ParseFile::parseWhirlgifBin(QString tmp, Scenarios *scenario) { scenario->setWhirlgifBinary(tmp.trimmed()); }

void ParseFile::parseECHO(QString tmp, Scenarios *scenario) { scenario->setECHO(str2bool(tmp.trimmed())); }

void ParseFile::parseWCF(QString tmp, Scenarios *scenario) { scenario->setWCF(str2bool(tmp.trimmed())); }

void ParseFile::parseWAF(QString tmp, Scenarios *scenario) { scenario->setWAF(str2bool(tmp.trimmed())); }

void ParseFile::parseWSDF(QString tmp, Scenarios *scenario) { scenario->setWSDF(str2bool(tmp.trimmed())); }

void ParseFile::parseWMM(QString tmp, Scenarios *scenario) { scenario->setWMM(str2bool(tmp.trimmed())); }

void ParseFile::parseLOGGING(QString tmp, Scenarios *scenario) { scenario->setLOGGING(str2bool(tmp.trimmed())); }

void ParseFile::parseLLS(QString tmp, Scenarios *scenario) { scenario->setLLS(str2bool(tmp.trimmed())); }

void ParseFile::parseLSW(QString tmp, Scenarios *scenario) { scenario->setLSW(str2bool(tmp.trimmed())); }

void ParseFile::parseLR(QString tmp, Scenarios *scenario) { scenario->setLR(str2bool(tmp.trimmed())); }

void ParseFile::parseLW(QString tmp, Scenarios *scenario) { scenario->setLW(str2bool(tmp.trimmed())); }

void ParseFile::parseLC(QString tmp, Scenarios *scenario) { scenario->setLC(str2bool(tmp.trimmed())); }

void ParseFile::parseLPS(QString tmp, Scenarios *scenario) { scenario->setLPS((enum LOGENUM)tmp.trimmed().toInt()); }

void ParseFile::parseLTM(QString tmp, Scenarios *scenario) { scenario->setLTM(str2bool(tmp.trimmed())); }

void ParseFile::parseLUA(QString tmp, Scenarios *scenario) { scenario->setLUA(str2bool(tmp.trimmed())); }

void ParseFile::parseLIC(QString tmp, Scenarios *scenario) { scenario->setLIC(str2bool(tmp.trimmed())); }

void ParseFile::parseLBS(QString tmp, Scenarios *scenario) { scenario->setLBS(str2bool(tmp.trimmed())); }

void ParseFile::parseLOGD(QString tmp, Scenarios *scenario) { scenario->setLOGD(str2bool(tmp.trimmed())); }

void ParseFile::parseLT(QString tmp, Scenarios *scenario) { scenario->setLT((enum LOGENUM)tmp.trimmed().toInt()); }

void ParseFile::parseNWG(QString tmp, Scenarios *scenario) { scenario->setNWG(tmp.trimmed().toInt()); }

void ParseFile::parseRS(QString tmp, Scenarios *scenario) { scenario->setRandSeed(tmp.trimmed().toInt()); }

void ParseFile::parseMCI(QString tmp, Scenarios *scenario) { scenario->setMCI(tmp.trimmed().toInt()); }

void ParseFile::parseCDSTA(QString tmp, Scenarios *scenario) { scenario->setCDSTA(tmp.trimmed().toInt()); }

void ParseFile::parseCDSTE(QString tmp, Scenarios *scenario) { scenario->setCDSTE(tmp.trimmed().toInt()); }

void ParseFile::parseCDSTO(QString tmp, Scenarios *scenario) { scenario->setCDSTO(tmp.trimmed().toInt()); }

void ParseFile::parseCBSTA(QString tmp, Scenarios *scenario) { scenario->setCBSTA(tmp.trimmed().toInt()); }

void ParseFile::parseCBSTE(QString tmp, Scenarios *scenario) { scenario->setCBSTE(tmp.trimmed().toInt()); }

void ParseFile::parseCBSTO(QString tmp, Scenarios *scenario) { scenario->setCBSTO(tmp.trimmed().toInt()); }

void ParseFile::parseCSSTA(QString tmp, Scenarios *scenario) { scenario->setCSSTA(tmp.trimmed().toInt()); }

void ParseFile::parseCSSTE(QString tmp, Scenarios *scenario) { scenario->setCSSTE(tmp.trimmed().toInt()); }

void ParseFile::parseCSSTO(QString tmp, Scenarios *scenario) { scenario->setCSSTO(tmp.trimmed().toInt()); }

void ParseFile::parseCSLSTA(QString tmp, Scenarios *scenario) { scenario->setCSLSTA(tmp.trimmed().toInt()); }

void ParseFile::parseCSLSTE(QString tmp, Scenarios *scenario) { scenario->setCSLSTE(tmp.trimmed().toInt()); }

void ParseFile::parseCSLSTO(QString tmp, Scenarios *scenario) { scenario->setCSLSTO(tmp.trimmed().toInt()); }

void ParseFile::parseCRSTA(QString tmp, Scenarios *scenario) { scenario->setCRSTA(tmp.trimmed().toInt()); }

void ParseFile::parseCRSTE(QString tmp, Scenarios *scenario) { scenario->setCRSTE(tmp.trimmed().toInt()); }

void ParseFile::parseCRSTO(QString tmp, Scenarios *scenario) { scenario->setCRSTO(tmp.trimmed().toInt()); }

void ParseFile::parsePDBF(QString tmp, Scenarios *scenario) { scenario->setPDBF(tmp.trimmed().toInt()); }

void ParseFile::parsePBBF(QString tmp, Scenarios *scenario) { scenario->setPBBF(tmp.trimmed().toInt()); }

void ParseFile::parsePSBF(QString tmp, Scenarios *scenario) { scenario->setPSBF(tmp.trimmed().toInt()); }

void ParseFile::parsePSLBF(QString tmp, Scenarios *scenario) { scenario->setPSLBF(tmp.trimmed().toInt()); }

void ParseFile::parsePRBF(QString tmp, Scenarios *scenario) { scenario->setPRBF(tmp.trimmed().toInt()); }

void ParseFile::parsePSTAD(QString tmp, Scenarios *scenario) { scenario->setPSTAD(tmp.trimmed().toInt()); }

void ParseFile::parsePSTOD(QString tmp, Scenarios *scenario) { scenario->setPSTOD(tmp.trimmed().toInt()); }

void ParseFile::parseUD(QString tmp, Scenarios *scenario) { scenario->setUD(tmp.trimmed()); }

void ParseFile::parseRD(QString tmp, Scenarios *scenario) { scenario->setRD(tmp.trimmed()); }

void ParseFile::parseLD(QString tmp, Scenarios *scenario) { scenario->setLD(tmp.trimmed()); }

void ParseFile::parseED(QString tmp, Scenarios *scenario) { scenario->setED(tmp.trimmed()); }

void ParseFile::parseSD(QString tmp, Scenarios *scenario) { scenario->setSD(tmp.trimmed()); }

void ParseFile::parseBD(QString tmp, Scenarios *scenario) { scenario->setBD(tmp.trimmed()); }

void ParseFile::parseWCKI(QString tmp, Scenarios *scenario) { scenario->setWCKI(str2bool(tmp.trimmed())); }

void ParseFile::parseEIF(QString tmp, Scenarios *scenario) { scenario->setEIF(str2bool(tmp.trimmed())); }

void ParseFile::parseANIMATION(QString tmp, Scenarios *scenario) { scenario->setANIMATION(str2bool(tmp.trimmed())); }

void ParseFile::parseDateColor(QString tmp, Scenarios *scenario) {
    scenario->setDateColor(MYUTILS::converStr2QC(tmp.simplified()));
}

void ParseFile::parseWaterColor(QString tmp, Scenarios *scenario) {
    scenario->setWaterColor(MYUTILS::converStr2QC(tmp.simplified()));
}

void ParseFile::parseSeedColor(QString tmp, Scenarios *scenario) {
    scenario->setSeedColor(MYUTILS::converStr2QC(tmp.simplified()));
}

void ParseFile::parseProbablityColor(QString tmp, Scenarios *scenario) {
    QStringList     splitList = tmp.trimmed().split(",");
    QString         cStr;
    ProbablityColor pc;
    pc.low  = splitList[0].simplified().toInt();
    pc.high = splitList[1].simplified().toInt();
    cStr    = splitList[2].simplified();
    if (cStr.size() == 0) {
        pc.isTransparent = true;
    } else {
        pc.color = MYUTILS::converStr2QC(cStr);
    }
    scenario->setProbablityColor(pc);
}

void ParseFile::parseLandClassColor(QString tmp, Scenarios *scenario) {
    QStringList splitList = tmp.trimmed().split(",");
    QString     cStr;
    LandClass   lcc;
    lcc.pix   = splitList[0].trimmed().toInt();
    lcc.name  = splitList[1].trimmed();
    lcc.flag  = splitList[2].trimmed();
    cStr      = splitList[3].simplified();
    lcc.color = MYUTILS::converStr2QC(cStr);
    scenario->setLandCColor(lcc);
}

PrintWindow ParseFile::parsePrintWindow(QString tmp) {
    QStringList pwList = tmp.trimmed().split(",");
    PrintWindow pw;
    pw.startRun        = pwList[0].trimmed().toInt();
    pw.endRun          = pwList[1].trimmed().toInt();
    pw.startMonteCarlo = pwList[2].trimmed().toInt();
    pw.endMonteCarlo   = pwList[3].trimmed().toInt();
    pw.startYear       = pwList[4].trimmed().toInt();
    pw.endYear         = pwList[5].trimmed().toInt();
    return pw;
}

void ParseFile::parseVGT(QString tmp, Scenarios *scenario) { scenario->setVGT(str2bool(tmp.trimmed())); }

void ParseFile::parseGPTW(QString tmp, Scenarios *scenario) { scenario->setGTPW(parsePrintWindow(tmp)); }

void ParseFile::parsePGC(QString tmp, Scenarios *scenario) { scenario->setPGC(MYUTILS::convertHex2QC(tmp.trimmed())); }

void ParseFile::parseVDA(QString tmp, Scenarios *scenario) { scenario->setVDA(str2bool(tmp.trimmed())); }

void ParseFile::parseDPW(QString tmp, Scenarios *scenario) { scenario->setDPW(parsePrintWindow(tmp)); }

void ParseFile::parseDC(QString tmp, Scenarios *scenario) {
    DeltatronColor dc;
    dc.dColors = MYUTILS::converStr2QC(tmp.simplified());
    scenario->setDC(dc);
}

void ParseFile::parseRGS(QString tmp, Scenarios *scenario) { scenario->setRGS(tmp.trimmed().toDouble()); }

void ParseFile::parseSS(QString tmp, Scenarios *scenario) { scenario->setSS(tmp.trimmed().toDouble()); }

void ParseFile::parseCL(QString tmp, Scenarios *scenario) { scenario->setCL(tmp.trimmed().toDouble()); }

void ParseFile::parseCH(QString tmp, Scenarios *scenario) { scenario->setCH(tmp.trimmed().toDouble()); }

void ParseFile::parseCS(QString tmp, Scenarios *scenario) { scenario->setCS(tmp.trimmed().toDouble()); }

void ParseFile::parseBOOM(QString tmp, Scenarios *scenario) { scenario->setBOOM(tmp.trimmed().toDouble()); }

void ParseFile::parseBUST(QString tmp, Scenarios *scenario) { scenario->setBUST(tmp.trimmed().toDouble()); }
