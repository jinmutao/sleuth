﻿#ifndef UTILS_H
#define UTILS_H
#include <QColor>
#include <QString>
namespace MYUTILS {
#define str2bool(str) str == "yes" || str == "YES" ? true : false
#define bool2str(b) b ? "YES" : "NO"
const QString ftpIp      = "47.108.153.97";
const QString userName   = "ftp";
const QString userPasswd = "Cqu1115";
const QString port       = "21";
#ifdef QT_NO_DEBUG
const QString program = "./grow.exe";
#else
const QString program = "E:/project/linux_project/qt_project/msleuth/grow.exe";
#endif
QString converRGB16HexStr(QColor _color, QString header = "0X");
QString converRGB16Hex(QColor);
QString converRGBStr(QColor);
QColor  convertHex2QC(QString hexStr);
///
/// \brief converStr2QC 将字符串转为QColor，这里自动判断是十六进制还是rgb字符串
/// \param colorStr
/// \return
///
QColor converStr2QC(QString colorStr);

/**
 * @brief getRelativeDirPath
 * @param dirPath
 * @return
 * 将绝对路径转为相对路径 E:\debug\msleuth -> .\debug\msleuth
 */
QString getRelativeDirPath(QString dirPath);
}  // namespace MYUTILS

#endif  // UTILS_H
