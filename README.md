| 开发环境 | 版本            |
| -------- | --------------- |
| QT       | 5.14.2          |
| Windows  | 10              |
| 编译环境 | qt自带的Mingw64 |

SLEUTH模型的界面版本，官方版本的SLEUTH模型需要安装cygwin，并手动编译出grow.exe，通过命令行使用。本仓库的软件是在官方的sleuth基础上，使用QT编写的一个易用版本的sleuth。

注意：仓库中去掉了三个文件夹`Input` 、`Output` 、`Scenarios` ,这三个文件可以从官方的源码中获取。 

## 改动

### 改变了预测模式下取起始土地利用数据的方法

文件位置：`growth.c` 函数 `grw_landuse_init`

```c++
// 如果模拟的起始年份小于最后一年土地利用数据的年份，则以最早的土地利用数据作为起点
// 比如有两个landuse.2002.gif和landuse.2018.gif
// 如果预测区间为[2002, 2018]则取02为起点
// 如果预测[2018, 1038],则取18为起点
if (igrid_GetLanduseYear(1) > scen_GetPredictionStartDate()) {
    printf("prediction_startYear(%d) < the year of the last landuse file(%d), there the seed year will be the first landuse file\n", scen_GetPredictionStartDate(), igrid_GetLanduseYear(1));
    landuse1_ptr = igrid_GetLanduseGridPtr (__FILE__, func, __LINE__, 0);
} else {
    printf("prediction_startYear(%d) >= the year of the last landuse file(%d), there the seed year will be the last landuse file\n", scen_GetPredictionStartDate(), igrid_GetLanduseYear(1));
    landuse1_ptr = igrid_GetLanduseGridPtr (__FILE__, func, __LINE__, 1);
}
```

