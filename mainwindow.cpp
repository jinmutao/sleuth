﻿#include "mainwindow.h"

#include <qprocess.h>

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <exception>

#include "sleuthlib/parse_file.h"
#include "sleuthlib/scenarios.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    connect(ui->actionDownload, SIGNAL(triggered()), this, SLOT(showFTPDialog()));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_rb_test_clicked() { this->mode = "test"; }

void MainWindow::on_rb_predict_clicked() { this->mode = "predict"; }

void MainWindow::on_rb_calibrate_clicked() { this->mode = "calibrate"; }

void MainWindow::on_rb_restart_clicked() { this->mode = "restart"; }

void MainWindow::on_btn_startSimulation_clicked() {
    ui->te_proResult->clear();
    Scenarios *scen = Scenarios::getInstance();
    if (!scen->getIsInited()) {
        ParseFile pf;
        scen = pf.file2Scenario(ui->le_scenarioPath->text().trimmed());
    }
    if (this->mode == "") {
        this->mode = "test";
    }
    // 验证输出目录的合法性
    //    correctOutputDir();
    ui->te_proResult->clear();
    ui->te_proResult->setText(QStringLiteral("开始模拟..."));
    ui->te_proResult->update();
    m_process = new QProcess(this);
    m_process->setProcessChannelMode(QProcess::MergedChannels);
    m_process->setReadChannel(QProcess::StandardOutput);
    connect(m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(setRes2Te()));
    connect(m_process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(slotProcessFinshed()));
    QString program = MYUTILS::program;
    program.append(" " + this->mode);
    program.append(" " + this->scenarioPath);
    if (this->scenarioPath == "") {
        ui->te_proResult->append(QString::fromLocal8Bit("wrong: 配置文件未找到!!!"));
    } else {
        qDebug() << program;
        m_process->start(program);
        qDebug() << m_process->state();
    }
    ui->btn_startSimulation->setEnabled(false);
}

void MainWindow::setRes2Te() {
    QByteArray qbt = m_process->readAllStandardOutput();
    QString    msg = QString::fromLocal8Bit(qbt);
    ui->te_proResult->append(msg);
    ui->te_proResult->update();
}

void MainWindow::on_btn_scenario_clicked() {
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, "Open File", "../", "Text File(*)");
    if (fileName == "") {
        return;
    }
    this->scenarioPath = fileName;
    ui->le_scenarioPath->setText(this->scenarioPath);
}

void MainWindow::on_btn_stopSimulation_clicked() {
    ui->btn_startSimulation->setEnabled(true);
    m_process->kill();
    ui->te_proResult->append("运行提前终止!!!");
}

////////////// tab2的表格按钮 /////////////////////////////
void MainWindow::on_tab2_btn_file_clicked() {
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, "Open File", "../", "Text File(*.log)");
    //    fileName = "C:/Users/Administrator/Desktop/打包/test/Output/demo200_cal/control_stats_pe_0.log";
    ui->tab2_leFilePath->setText(fileName);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::critical(this, "错误提示", "文件读取失败！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    // 跳过第一行
    file.readLine();
    // 读取表头
    QString str(file.readLine());
    // 将多个空格合为一个
    str.replace(QRegExp("[\\s]+"), " ");
    QStringList headers = str.split(" ");
    headers.removeFirst();
    headers.removeLast();
    headers << "OSM";
    qDebug() << headers;
    QList<QStringList> datas;
    while (!file.atEnd()) {
        str = file.readLine();
        str = str.trimmed();
        str.replace(QRegExp("[\\s]+"), " ");
        datas << str.split(" ");
    }

    /////////// 创建表格 //////////////
    // 清空表格
    int rowCount = ui->table_stats->rowCount();
    for (int i = 0; i < rowCount; i++) {
        ui->table_stats->removeRow(0);
    }

    int rows = datas.size();
    int cols = headers.size();
    // 创建表头
    ui->table_stats->setSortingEnabled(true);
    ui->table_stats->setColumnCount(cols);
    ui->table_stats->setRowCount(rows);
    ui->table_stats->setHorizontalHeaderLabels(headers);
    ui->table_stats->setSelectionBehavior(QAbstractItemView::SelectRows);   //整行选中的方式
    ui->table_stats->setEditTriggers(QAbstractItemView::NoEditTriggers);    //禁止修改
    ui->table_stats->setSelectionMode(QAbstractItemView::SingleSelection);  //设置为可以选中单个
    ui->table_stats->verticalHeader()->setVisible(false);                   //隐藏列表头
    double     osm;
    QList<int> osmMetricIndex = {2, 3, 4, 5, 8, 10, 11};
    QString    valStr;
    int        pos = 8;
    // 填充数据
    for (int i = 0; i < rows; i++) {
        osm = 1.0;
        for (int j = 0; j < cols - 1; j++) {
            valStr = datas.at(i)[j];
            if (osmMetricIndex.contains(j)) {
                osm *= valStr.toDouble();
            }
            ui->table_stats->setItem(i, j, new QTableWidgetItem(valStr));
        }
        ui->table_stats->setItem(i, cols - 1, new QTableWidgetItem(QString::number(osm, 'f', pos)));
    }

    // 填充数据

    file.close();
}

/**
 * @brief MainWindow::on_btn_editScenario_clicked
 * 编辑scenario文件按钮
 */
void MainWindow::on_btn_editScenario_clicked() {
    //    QFile file();
    QString filePath = ui->le_scenarioPath->text();
    //    QString filePath = "E:/project/打包/test/Scenarios/scenariobak.demo200_test";
    QFile file(filePath);
    if (!file.exists()) {
        return;
    }
    scenarioForm = new ScenarioForm();
    scenarioForm->setWindowFlags(Qt::Widget);
    scenarioForm->setModal(true);
    scenarioForm->setAttribute(Qt::WA_DeleteOnClose);
    scenarioForm->initForm(filePath);
    scenarioForm->show();
}

void MainWindow::correctOutputDir() {
    Scenarios *scen      = Scenarios::getInstance();
    QString    outputDir = scen->getOutputDir();
    // 判断输出目录最后是否有 /
    if (outputDir[outputDir.length() - 1] != "/") {
        outputDir += "/";
        scen->setOutputDir(outputDir);
        ParseFile pf;
        pf.Scenario2file(ui->le_scenarioPath->text().trimmed());
    }
    QStringList fileNames;
    QStringList filePaths;
    if (mode == "calibrate") {
        fileNames = TEMFILES::calFiles;
        filePaths = TEMFILES::calFilePath;
    } else if (mode == "test") {
        fileNames = TEMFILES::testFiles;
        filePaths = TEMFILES::testFilePath;
    } else {
        fileNames = TEMFILES::preFiles;
        filePaths = TEMFILES::preFilePath;
    }
    QDir directory(outputDir);
    if (!directory.exists()) {
        qDebug() << 123;
        bool ismkdir = QDir().mkpath(outputDir);
        qDebug() << ismkdir;
    }
    return;
    QStringList fileNameList = directory.entryList();
    int         i, len = fileNames.size();
    QString     dstFilePath;
    for (i = 0; i < len; i++) {
        if (!fileNameList.contains(fileNames[i])) {
            dstFilePath = outputDir + fileNames[i];
            QFile::setPermissions(dstFilePath, QFile::WriteOwner);
            bool r = QFile::copy(filePaths[i], dstFilePath);
            if (!r) {
                qDebug() << "复制文件出错";
            }
        }
    }
}

QString MainWindow::getScenarioPath() { return scenarioPath; }

void MainWindow::scenarioForm2Main() {
    scenarioForm->hide();
    this->show();
}

void MainWindow::slotProcessFinshed() {
    ui->btn_startSimulation->setEnabled(true);
    ui->te_proResult->append(QString::fromLocal8Bit("模拟完成!!!!"));
}

void MainWindow::showFTPDialog() {
    FtpDialog *ad = new FtpDialog();
    ad->setWindowFlags(Qt::Widget);
    //    id->setModal(true);
    ad->setAttribute(Qt::WA_DeleteOnClose);
    ad->show();
}
