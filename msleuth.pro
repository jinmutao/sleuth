QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    dialog.cpp \
    mainwindow.cpp \
    scenarioform.cpp \
    sleuthlib/callexe.cpp \
    sleuthlib/parse_file.cpp \
    sleuthlib/scenarios.cpp \
    sleuthlib/test.cpp \
    sleuthlib/utils.cpp \
    thirdPart/customWidget/CustomLineEdit/CustomLineEdit.cpp \
    thirdPart/customWidget/CustomLineEdit/LineEditpart.cpp \
    thirdPart/customWidget/LEDNumber/LedNumber.cpp \
    thirdPart/customWidget/Loading/Loading.cpp \
    thirdPart/customWidget/RoundDot/RoundDot.cpp \
    thirdPart/customWidget/RoundProgressBar/roundprogressbar.cpp \
    thirdPart/customWidget/SelfAdaptableLabel/SelfAdaptableLabel.cpp \
    thirdPart/ftp/ftpdialog.cpp \
    thirdPart/ftp/loadingform.cpp

HEADERS += \
    dialog.h \
    mainwindow.h \
    scenarioform.h \
    sleuthlib/callexe.h \
    sleuthlib/hints.h \
    sleuthlib/parse_file.h \
    sleuthlib/scenarioStruct.h \
    sleuthlib/scenarios.h \
    sleuthlib/test.h \
    sleuthlib/utils.h \
    thirdPart/customWidget/CustomLineEdit/CustomLineEdit.h \
    thirdPart/customWidget/CustomLineEdit/LineEditpart.h \
    thirdPart/customWidget/LEDNumber/LedNumber.h \
    thirdPart/customWidget/Loading/Loading.h \
    thirdPart/customWidget/RoundDot/RoundDot.h \
    thirdPart/customWidget/RoundProgressBar/roundprogressbar.h \
    thirdPart/customWidget/SelfAdaptableLabel/SelfAdaptableLabel.h \
    thirdPart/ftp/ftpdialog.h \
    thirdPart/ftp/loadingform.h


FORMS += \
    dialog.ui \
    mainwindow.ui \
    scenarioform.ui \
    thirdPart/ftp/ftpdialog.ui \
    thirdPart/ftp/loadingform.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target



RESOURCES += \
    res.qrc
RC_FILE = myapp.rc
#QMAKE_CXXFLAGS +=  -Wno-unused-parameter



win32:CONFIG(release, debug|release): LIBS += -LF:/devEnv/build-qtftp-Desktop_Qt_5_14_2_MSVC2017_64bit-Debug/lib/ -lQt5Ftp
else:win32:CONFIG(debug, debug|release): LIBS += -LF:/devEnv/build-qtftp-Desktop_Qt_5_14_2_MSVC2017_64bit-Debug/lib/ -lQt5Ftpd

INCLUDEPATH += F:/devEnv/qtftp/src/qftp
DEPENDPATH += F:/devEnv/qtftp/src/qftp
