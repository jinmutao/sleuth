#ifndef PARSE_FILE_H
#define PARSE_FILE_H
#include <QDebug>
#include <QFile>
#include <QMap>

#include "hints.h"
#include "scenarioStruct.h"
#include "scenarios.h"
#include "utils.h"

class ParseFile {
public:
    typedef void (ParseFile::*Ptrtype)(QString, Scenarios *);
    ParseFile();
    ~ParseFile();

    Scenarios *file2Scenario(QString filePath);
    bool       Scenario2file(QString filePath);

    void parseLine(QString tmp, Scenarios *scenario = nullptr);
    void parseInputDir(QString tmp, Scenarios *scenario);
    void parseOutputDir(QString tmp, Scenarios *scenario);
    void parseWhirlgifBin(QString tmp, Scenarios *scenario);
    void parseECHO(QString tmp, Scenarios *scenario);

    void parseWCF(QString tmp, Scenarios *scenario);
    void parseWAF(QString tmp, Scenarios *scenario);
    void parseWSDF(QString tmp, Scenarios *scenario);
    void parseWMM(QString tmp, Scenarios *scenario);
    void parseLOGGING(QString tmp, Scenarios *scenario);

    void parseLLS(QString tmp, Scenarios *scenario);
    void parseLSW(QString tmp, Scenarios *scenario);
    void parseLR(QString tmp, Scenarios *scenario);
    void parseLW(QString tmp, Scenarios *scenario);
    void parseLC(QString tmp, Scenarios *scenario);
    void parseLPS(QString tmp, Scenarios *scenario);
    void parseLTM(QString tmp, Scenarios *scenario);
    void parseLUA(QString tmp, Scenarios *scenario);
    void parseLIC(QString tmp, Scenarios *scenario);
    void parseLBS(QString tmp, Scenarios *scenario);
    void parseLOGD(QString tmp, Scenarios *scenario);
    void parseLT(QString tmp, Scenarios *scenario);

    void parseNWG(QString tmp, Scenarios *scenario);
    void parseRS(QString tmp, Scenarios *scenario);
    void parseMCI(QString tmp, Scenarios *scenario);

    /// ============== //////
    void parseCDSTA(QString tmp, Scenarios *scenario);
    void parseCDSTE(QString tmp, Scenarios *scenario);
    void parseCDSTO(QString tmp, Scenarios *scenario);

    void parseCBSTA(QString tmp, Scenarios *scenario);
    void parseCBSTE(QString tmp, Scenarios *scenario);
    void parseCBSTO(QString tmp, Scenarios *scenario);

    void parseCSSTA(QString tmp, Scenarios *scenario);
    void parseCSSTE(QString tmp, Scenarios *scenario);
    void parseCSSTO(QString tmp, Scenarios *scenario);

    void parseCSLSTA(QString tmp, Scenarios *scenario);
    void parseCSLSTE(QString tmp, Scenarios *scenario);
    void parseCSLSTO(QString tmp, Scenarios *scenario);

    void parseCRSTA(QString tmp, Scenarios *scenario);
    void parseCRSTE(QString tmp, Scenarios *scenario);
    void parseCRSTO(QString tmp, Scenarios *scenario);

    void parsePDBF(QString tmp, Scenarios *scenario);
    void parsePBBF(QString tmp, Scenarios *scenario);
    void parsePSBF(QString tmp, Scenarios *scenario);
    void parsePSLBF(QString tmp, Scenarios *scenario);
    void parsePRBF(QString tmp, Scenarios *scenario);

    void parsePSTAD(QString tmp, Scenarios *scenario);
    void parsePSTOD(QString tmp, Scenarios *scenario);

    void parseUD(QString tmp, Scenarios *scenario);
    void parseRD(QString tmp, Scenarios *scenario);
    void parseLD(QString tmp, Scenarios *scenario);
    void parseED(QString tmp, Scenarios *scenario);
    void parseSD(QString tmp, Scenarios *scenario);
    void parseBD(QString tmp, Scenarios *scenario);

    //// # XI. OUTPUT IMAGES  ////
    void parseWCKI(QString tmp, Scenarios *scenario);
    void parseEIF(QString tmp, Scenarios *scenario);
    void parseANIMATION(QString tmp, Scenarios *scenario);

    //// # XII. COLORTABLE SETTINGS ////
    void parseDateColor(QString tmp, Scenarios *scenario);
    void parseWaterColor(QString tmp, Scenarios *scenario);
    void parseSeedColor(QString tmp, Scenarios *scenario);

    void parseProbablityColor(QString tmp, Scenarios *scenario);
    void parseLandClassColor(QString tmp, Scenarios *scenario);

    PrintWindow parsePrintWindow(QString tmp);

    //  D. GROWTH TYPE IMAGE OUTPUT CONTROL AND COLORTABLE
    void parseVGT(QString tmp, Scenarios *scenario);
    void parseGPTW(QString tmp, Scenarios *scenario);
    void parsePGC(QString tmp, Scenarios *scenario);

    //  E. DELTATRON AGING SECTION
    void parseVDA(QString tmp, Scenarios *scenario);
    void parseDPW(QString tmp, Scenarios *scenario);
    void parseDC(QString tmp, Scenarios *scenario);

    // XIII. SELF-MODIFICATION PARAMETERS
    void parseRGS(QString tmp, Scenarios *scenario);
    void parseSS(QString tmp, Scenarios *scenario);
    void parseCL(QString tmp, Scenarios *scenario);
    void parseCH(QString tmp, Scenarios *scenario);
    void parseCS(QString tmp, Scenarios *scenario);
    void parseBOOM(QString tmp, Scenarios *scenario);
    void parseBUST(QString tmp, Scenarios *scenario);

private:
    Ptrtype _pmf;  //定义了一个 Ptrtype 类型的变量：_pmf，是一个指向类成员函数的指针

    QMap<QString, Ptrtype> func_map;  //定义一个map。key为index，value为index对应的Ptrtype类型的类成员函数
};

#endif  // PARSE_FILE_H
