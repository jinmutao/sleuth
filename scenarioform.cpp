﻿#include "scenarioform.h"

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

#include "sleuthlib/parse_file.h"
#include "ui_scenarioform.h"

ScenarioForm::ScenarioForm(QWidget *parent) : QDialog(parent), ui(new Ui::ScenarioForm) {
    ui->setupUi(this);
    changed    = false;
    imageTypes = new QStringList(
        {"URBAN_DATA", "ROAD_DATA", "LANDUSE_DATA", "EXCLUDED_DATA", "SLOPE_DATA", "BACKGROUND_DATA"});
    flagTypes = new QStringList({"", "UNC", "URB", "EXC"});
    scen      = Scenarios::getInstance();
}

ScenarioForm::~ScenarioForm() { delete ui; }

void ScenarioForm::connectSlots() {
    connect(ui->tab_edit, SIGNAL(currentChanged(int)), this, SLOT(show_save_hint()));
    connect(ui->te_scenarioContents, SIGNAL(textChanged()), this, SLOT(hasChanged()));

    connect(
        ui->tw_comColors, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(colorCellClicked(QTableWidgetItem *)));
    connect(ui->tw_proTable, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(colorCellClicked(QTableWidgetItem *)));
    connect(
        ui->tw_gColorTable, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(colorCellClicked(QTableWidgetItem *)));
    connect(
        ui->tw_dColorTable, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(colorCellClicked(QTableWidgetItem *)));

    connect(ui->le_sinputDir, SIGNAL(textChanged(QString)), this, SLOT(hasChanged()));
    connect(ui->le_soutputDIr, SIGNAL(textChanged(QString)), this, SLOT(hasChanged()));
    connect(ui->le_sWhBin, SIGNAL(textChanged(QString)), this, SLOT(hasChanged()));

    connect(ui->cb_secho, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_swcf, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_swaf, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_wsdf, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_swmm, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slogging, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));

    connect(ui->cb_slls, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slsw, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slr, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slw, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slc, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_sltm, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slua, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slogd, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_lic, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slbs, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slps, SIGNAL(currentIndexChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_slt, SIGNAL(currentIndexChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_snwg, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_srandSeed, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_smci, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    // paramters
    connect(ui->sb_sDiffusionSta, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDiffusionSte, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDiffusionSto, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDiffusionBest, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sBreedSta, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sBreedSte, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sBreedSto, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sBreedBest, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sSpreadSta, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSpreadSte, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSpreadSto, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSpreadBest, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sSlopeSta, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSlopeSte, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSlopeSto, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSlopeBest, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sRoadSta, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sRoadSte, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sRoadSto, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sRoadBest, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sPSTAD, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sPSTOD, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->cb_sWCKI, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_sEIF, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->cb_sANIMATION, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));

    connect(ui->cb_sVGT, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sStartRun, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sEndRun, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sSMC, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sEMC, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sStartYear, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sEndYear, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->cb_sVDA, SIGNAL(stateChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDStartRun, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDEndRun, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDSMC, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDEMC, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDStartYear, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));
    connect(ui->sb_sDEndYear, SIGNAL(valueChanged(int)), this, SLOT(hasChanged()));

    connect(ui->sb_sRGS, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sSS, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sCL, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sCH, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sCS, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sBOOM, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));
    connect(ui->sb_sBUST, SIGNAL(valueChanged(double)), this, SLOT(hasChanged()));

    connect(ui->btn_addImage, SIGNAL(clicked()), this, SLOT(hasChanged()));
    connect(ui->btn_addDColor, SIGNAL(clicked()), this, SLOT(hasChanged()));
    connect(ui->btn_addLandColor, SIGNAL(clicked()), this, SLOT(hasChanged()));
    connect(ui->btn_addProbColor, SIGNAL(clicked()), this, SLOT(hasChanged()));
}

void ScenarioForm::updateScenario() {
    scen->setInputDir(ui->le_sinputDir->text());
    scen->setOutputDir(ui->le_soutputDIr->text());
    scen->setWhirlgifBinary(ui->le_sWhBin->text());

    scen->setECHO(ui->cb_secho->checkState());

    scen->setWCF(ui->cb_swcf->checkState());
    scen->setWAF(ui->cb_swaf->checkState());
    scen->setWSDF(ui->cb_wsdf->checkState());
    scen->setWMM(ui->cb_swmm->checkState());
    scen->setLOGGING(ui->cb_slogging->checkState());

    scen->setLLS(ui->cb_slls->checkState());
    scen->setLSW(ui->cb_slsw->checkState());
    scen->setLR(ui->cb_slr->checkState());
    scen->setLW(ui->cb_slw->checkState());
    scen->setLC(ui->cb_slc->checkState());
    scen->setLTM(ui->cb_sltm->checkState());
    scen->setLUA(ui->cb_slua->checkState());
    scen->setLOGD(ui->cb_slogd->checkState());
    scen->setLIC(ui->cb_lic->checkState());
    scen->setLBS(ui->cb_slbs->checkState());
    scen->setLPS((LOGENUM)ui->cb_slps->currentIndex());
    scen->setLT((LOGENUM)ui->cb_slt->currentIndex());

    scen->setNWG(ui->sb_snwg->value());
    scen->setRandSeed(ui->sb_srandSeed->value());
    scen->setMCI(ui->sb_smci->value());

    scen->setCDSTA(ui->sb_sDiffusionSta->value());
    scen->setCDSTE(ui->sb_sDiffusionSte->value());
    scen->setCDSTO(ui->sb_sDiffusionSto->value());
    scen->setPDBF(ui->sb_sDiffusionBest->value());

    scen->setCBSTA(ui->sb_sBreedSta->value());
    scen->setCBSTE(ui->sb_sBreedSte->value());
    scen->setCBSTO(ui->sb_sBreedSto->value());
    scen->setPBBF(ui->sb_sBreedBest->value());

    scen->setCSSTA(ui->sb_sSpreadSta->value());
    scen->setCSSTE(ui->sb_sSpreadSte->value());
    scen->setCSSTO(ui->sb_sSpreadSto->value());
    scen->setPSBF(ui->sb_sSpreadBest->value());

    scen->setCSLSTA(ui->sb_sSlopeSta->value());
    scen->setCSLSTE(ui->sb_sSlopeSte->value());
    scen->setCSLSTO(ui->sb_sSlopeSto->value());
    scen->setPSLBF(ui->sb_sSlopeBest->value());

    scen->setCRSTA(ui->sb_sRoadSta->value());
    scen->setCRSTE(ui->sb_sRoadSte->value());
    scen->setCRSTO(ui->sb_sRoadSto->value());
    scen->setPRBF(ui->sb_sRoadBest->value());

    scen->setPSTAD(ui->sb_sPSTAD->value());
    scen->setPSTOD(ui->sb_sPSTOD->value());

    scen->setWCKI(ui->cb_sWCKI->checkState());
    scen->setEIF(ui->cb_sEIF->checkState());
    scen->setANIMATION(ui->cb_sANIMATION->checkState());

    PrintWindow *pw = new PrintWindow();
    scen->setVGT(ui->cb_sVGT->checkState());
    pw->startRun        = ui->sb_sStartRun->value();
    pw->endRun          = ui->sb_sEndRun->value();
    pw->startMonteCarlo = ui->sb_sSMC->value();
    pw->endMonteCarlo   = ui->sb_sEMC->value();
    pw->startYear       = ui->sb_sStartYear->value();
    pw->endYear         = ui->sb_sEndYear->value();
    scen->setGTPW(*pw);

    pw = new PrintWindow();
    scen->setVDA(ui->cb_sVDA->checkState());
    pw->startRun        = ui->sb_sDStartRun->value();
    pw->endRun          = ui->sb_sDEndRun->value();
    pw->startMonteCarlo = ui->sb_sDSMC->value();
    pw->endMonteCarlo   = ui->sb_sDEMC->value();
    pw->startYear       = ui->sb_sDStartYear->value();
    pw->endYear         = ui->sb_sDEndYear->value();
    scen->setDPW(*pw);

    scen->setRGS(ui->sb_sRGS->value());
    scen->setSS(ui->sb_sSS->value());
    scen->setCL(ui->sb_sCL->value());
    scen->setCH(ui->sb_sCH->value());
    scen->setCS(ui->sb_sCS->value());
    scen->setBOOM(ui->sb_sBOOM->value());
    scen->setBUST(ui->sb_sBUST->value());

    // 清空List，重新赋值
    scen->clearAllList();
    int rowNum;
    // ============ 获取tw_images表格数据 ====================
    rowNum = ui->tw_images->rowCount();
    QComboBox *cb;
    QString    tmp;
    for (int i = 0; i < rowNum; i++) {
        cb  = static_cast<QComboBox *>(ui->tw_images->cellWidget(i, 0));
        tmp = cb->currentText();
        if (tmp == "URBAN_DATA") {
            scen->setUD(ui->tw_images->item(i, 1)->text());
        } else if (tmp == "ROAD_DATA") {
            scen->setRD(ui->tw_images->item(i, 1)->text());
        } else if (tmp == "LANDUSE_DATA") {
            scen->setLD(ui->tw_images->item(i, 1)->text());
        } else if (tmp == "EXCLUDED_DATA") {
            scen->setED(ui->tw_images->item(i, 1)->text());
        } else if (tmp == "SLOPE_DATA") {
            scen->setSD(ui->tw_images->item(i, 1)->text());
        } else if (tmp == "BACKGROUND_DATA") {
            scen->setBD(ui->tw_images->item(i, 1)->text());
        }
    }

    // ============ 获取tw_images表格数据 ====================
    rowNum = ui->tw_comColors->rowCount();
    for (int i = 0; i < rowNum; i++) {
        tmp = ui->tw_comColors->item(i, 0)->text();
        if (tmp == "DATE_COLOR") {
            scen->setDateColor(MYUTILS::converStr2QC(ui->tw_comColors->item(i, 3)->text().trimmed()));
        } else if (tmp == "SEED_COLOR") {
            scen->setSeedColor(MYUTILS::converStr2QC(ui->tw_comColors->item(i, 3)->text().trimmed()));
        } else if (tmp == "WATER_COLOR") {
            scen->setWaterColor(MYUTILS::converStr2QC(ui->tw_comColors->item(i, 3)->text().trimmed()));
        }
    }

    // ============ 获取tw_proTable表格数据 ====================
    // 排序
    ui->tw_proTable->sortByColumn(1, Qt::AscendingOrder);
    rowNum = ui->tw_proTable->rowCount();
    for (int i = 0; i < rowNum; i++) {
        ProbablityColor pc;
        pc.low  = ui->tw_proTable->item(i, 1)->text().toInt();
        pc.high = ui->tw_proTable->item(i, 2)->text().toInt();
        tmp     = ui->tw_proTable->item(i, 5)->text().trimmed();
        if (tmp == "") {
            pc.isTransparent = true;
        } else {
            pc.color = MYUTILS::converStr2QC(tmp);
        }
        scen->setProbablityColor(pc);
    }

    // ============ 获取tw_landColorTable表格数据 ====================
    // 按照pix排序
    ui->tw_landColorTable->sortByColumn(1, Qt::AscendingOrder);
    rowNum = ui->tw_landColorTable->rowCount();
    for (int i = 0; i < rowNum; i++) {
        LandClass lc;
        lc.pix   = ui->tw_landColorTable->item(i, 1)->text().toInt();
        lc.name  = ui->tw_landColorTable->item(i, 2)->text();
        cb       = static_cast<QComboBox *>(ui->tw_landColorTable->cellWidget(i, 3));
        lc.flag  = cb->currentText();
        tmp      = ui->tw_landColorTable->item(i, 6)->text().trimmed();
        lc.color = MYUTILS::converStr2QC(tmp);
        scen->setLandCColor(lc);
    }

    // ============ 获取tw_gColorTable表格数据 ====================
    rowNum = ui->tw_gColorTable->rowCount();
    for (int i = 0; i < rowNum; i++) {
        tmp = ui->tw_gColorTable->item(i, 3)->text().trimmed();
        scen->setPGC(MYUTILS::converStr2QC(tmp));
    }

    // ============ 获取tw_dColorTable表格数据 ====================
    rowNum = ui->tw_dColorTable->rowCount();
    for (int i = 0; i < rowNum; i++) {
        DeltatronColor dc;
        tmp        = ui->tw_dColorTable->item(i, 3)->text().trimmed();
        dc.dColors = MYUTILS::converStr2QC(tmp);
        scen->setDC(dc);
    }
}

void ScenarioForm::loadData2Tab2() {
    scen->clearAllList();
    ParseFile pf;
    if (pf.file2Scenario(scenarioPath) == nullptr) {
        QMessageBox::critical(this, "错误提示", "文件读取失败！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }

    // 加载表格意外的其他数据

    ui->le_sinputDir->setText(scen->getInputDir());
    ui->le_soutputDIr->setText(scen->getOutputDir());
    ui->le_sWhBin->setText(scen->getWhirlgifBinary());

    ui->cb_secho->setChecked(scen->getECHO());

    ui->cb_swcf->setChecked(scen->getWCF());
    ui->cb_swaf->setChecked(scen->getWAF());
    ui->cb_wsdf->setChecked(scen->getWSDF());
    ui->cb_swmm->setChecked(scen->getWMM());
    ui->cb_slogging->setChecked(scen->getLOGGING());

    ui->cb_slls->setChecked(scen->getLLS());
    ui->cb_slsw->setChecked(scen->getLSW());
    ui->cb_slr->setChecked(scen->getLR());
    ui->cb_slw->setChecked(scen->getLW());
    ui->cb_slc->setChecked(scen->getLC());
    ui->cb_sltm->setChecked(scen->getLTM());
    ui->cb_slua->setChecked(scen->getLUA());
    ui->cb_slogd->setChecked(scen->getLOGD());
    ui->cb_lic->setChecked(scen->getLIC());
    ui->cb_slbs->setChecked(scen->getLBS());
    ui->cb_slps->setCurrentIndex((int)scen->getLPS());
    ui->cb_slt->setCurrentIndex((int)scen->getLT());

    ui->sb_snwg->setValue(scen->getNWG());
    ui->sb_srandSeed->setValue(scen->getRandSeed());
    ui->sb_smci->setValue(scen->getMCI());

    ui->sb_sDiffusionSta->setValue(scen->getCDSTA());
    ui->sb_sDiffusionSte->setValue(scen->getCDSTE());
    ui->sb_sDiffusionSto->setValue(scen->getCDSTO());
    ui->sb_sDiffusionBest->setValue(scen->getPDBF());

    ui->sb_sBreedSta->setValue(scen->getCBSTA());
    ui->sb_sBreedSte->setValue(scen->getCBSTE());
    ui->sb_sBreedSto->setValue(scen->getCBSTO());
    ui->sb_sBreedBest->setValue(scen->getPBBF());

    ui->sb_sSpreadSta->setValue(scen->getCSSTA());
    ui->sb_sSpreadSte->setValue(scen->getCSSTE());
    ui->sb_sSpreadSto->setValue(scen->getCSSTO());
    ui->sb_sSpreadBest->setValue(scen->getPSBF());

    ui->sb_sSlopeSta->setValue(scen->getCSLSTA());
    ui->sb_sSlopeSte->setValue(scen->getCSLSTE());
    ui->sb_sSlopeSto->setValue(scen->getCSLSTO());
    ui->sb_sSlopeBest->setValue(scen->getPSLBF());

    ui->sb_sRoadSta->setValue(scen->getCRSTA());
    ui->sb_sRoadSte->setValue(scen->getCRSTE());
    ui->sb_sRoadSto->setValue(scen->getCRSTO());
    ui->sb_sRoadBest->setValue(scen->getPRBF());

    ui->sb_sPSTAD->setValue(scen->getPSTAD());
    ui->sb_sPSTOD->setValue(scen->getPSTOD());

    ui->cb_sWCKI->setChecked(scen->getWCKI());
    ui->cb_sEIF->setChecked(scen->getEIF());
    ui->cb_sANIMATION->setChecked(scen->getANIMATION());

    ui->cb_sVGT->setChecked(scen->getVGT());
    PrintWindow pw = scen->getGTPW();
    ui->sb_sStartRun->setValue(pw.startRun);
    ui->sb_sEndRun->setValue(pw.endRun);
    ui->sb_sSMC->setValue(pw.startMonteCarlo);
    ui->sb_sEMC->setValue(pw.endMonteCarlo);
    ui->sb_sStartYear->setValue(pw.startYear);
    ui->sb_sEndYear->setValue(pw.endYear);

    ui->cb_sVDA->setChecked(scen->getVDA());
    pw = scen->getDPW();
    ui->sb_sDStartRun->setValue(pw.startRun);
    ui->sb_sDEndRun->setValue(pw.endRun);
    ui->sb_sDSMC->setValue(pw.startMonteCarlo);
    ui->sb_sDEMC->setValue(pw.endMonteCarlo);
    ui->sb_sDStartYear->setValue(pw.startYear);
    ui->sb_sDEndYear->setValue(pw.endYear);

    ui->sb_sRGS->setValue(scen->getRGS());
    ui->sb_sSS->setValue(scen->getSS());
    ui->sb_sCL->setValue(scen->getCL());
    ui->sb_sCH->setValue(scen->getCH());
    ui->sb_sCS->setValue(scen->getCS());
    ui->sb_sBOOM->setValue(scen->getBOOM());
    ui->sb_sBUST->setValue(scen->getBUST());

    initTable();
    connect(ui->tw_images, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    connect(ui->tw_proTable, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    connect(ui->tw_comColors, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    connect(ui->tw_dColorTable, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    connect(ui->tw_gColorTable, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    connect(ui->tw_landColorTable, SIGNAL(cellChanged(int, int)), this, SLOT(hasChanged()));
    // 当加载数据时，不算是做了修改
    changed = false;
}

void ScenarioForm::setScenarioContents(QString contents) {
    if (contents == nullptr) {
        QFile file(scenarioPath);
        if (!file.open(QFile::ReadOnly | QFile::Text)) {
            QMessageBox::critical(
                this, "错误提示", "文件读取失败！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return;
        }
        ui->te_scenarioContents->setPlainText(file.readAll());
        file.close();
    } else {
        ui->te_scenarioContents->setPlainText(contents);
    }
    changed = false;
}

void ScenarioForm::setScenarioPath(QString tmp) { scenarioPath = tmp; }

void ScenarioForm::on_scenario_save_clicked() {
    tab1_save();
    loadData2Tab2();
    changed = false;
}

void ScenarioForm::setScenarioDetail(QStringList sd) {
    QStringListIterator iter(sd);
    while (iter.hasNext()) {
        qDebug() << iter.next().toLocal8Bit().constData();
    }
}

void ScenarioForm::initForm(QString path) {
    setScenarioPath(path);
    setScenarioContents();
    loadData2Tab2();
    connectSlots();
}

void ScenarioForm::closeEvent(QCloseEvent *event) {
    if (!changed) {
        return;
    }
    if (ui->tab_edit->currentIndex() == 1) {
        if (QMessageBox::Save
            == QMessageBox::warning(this, QString::fromLocal8Bit("配置文件保存"),
                QString::fromLocal8Bit("保存配置文件?"), QMessageBox::Save | QMessageBox::Cancel, QMessageBox::Save)) {
            ParseFile pf;
            updateScenario();
            pf.Scenario2file(scenarioPath);
            changed = false;
        }
    } else {  // 0 -> 1
        if (QMessageBox::Save
            == QMessageBox::warning(this, QString::fromLocal8Bit("配置文件保存"),
                QString::fromLocal8Bit("保存配置文件?"), QMessageBox::Save | QMessageBox::Cancel, QMessageBox::Save)) {
            tab1_save();
            changed = false;
        }
    }
}

void ScenarioForm::show_save_hint() {
    if (!changed) {
        return;
    }
    // 1 -> 0
    if (ui->tab_edit->currentIndex() == 0) {
        if (QMessageBox::Save
            == QMessageBox::warning(this, QString::fromLocal8Bit("配置文件保存"),
                QString::fromLocal8Bit("保存配置文件?"), QMessageBox::Save | QMessageBox::Cancel, QMessageBox::Save)) {
            ParseFile pf;
            updateScenario();
            bool status = pf.Scenario2file(scenarioPath);
            if (!status) {
                QMessageBox::critical(this, QString::fromLocal8Bit("提示信息"), QString::fromLocal8Bit("配置保存错误"),
                    QMessageBox::Ok, QMessageBox::Ok);
                return;
            }
            ui->te_scenarioContents->clear();
            setScenarioContents();
            ui->te_scenarioContents->update();
            changed = false;
        }
    } else {  // 0 -> 1
        if (QMessageBox::Save
            == QMessageBox::warning(this, QString::fromLocal8Bit("配置文件保存"),
                QString::fromLocal8Bit("保存配置文件?"), QMessageBox::Save | QMessageBox::Cancel, QMessageBox::Save)) {
            tab1_save();
            changed = false;
            loadData2Tab2();
        }
    }
}

void ScenarioForm::initTable() {
    /////////// 创建表格 //////////////
    initImageTable();
    initColorTable();
}

/**
 * @brief ScenarioForm::initColorTable
 * 初始化颜色表格
 */
void ScenarioForm::initColorTable() {
    Scenarios *scen = Scenarios::getInstance();
    ////////////// 初始化common colors //////////////////////
    QStringList   cct = {"DATE_COLOR", "SEED_COLOR", "WATER_COLOR"};
    QList<QColor> colorList;
    if (scen->getIsInited()) {
        colorList << scen->getDateColor() << scen->getSeedColor() << scen->getWaterColor();
    } else {
        colorList << QColor(200, 200, 200) << QColor(200, 200, 200) << QColor(200, 200, 200);
    }
    QStringList       headers = {"CommonColorsTypes", "color", "rgb", "hex"};
    QTableWidgetItem *tmp;
    ui->tw_comColors->setRowCount(cct.size());
    ui->tw_comColors->setColumnCount(headers.size());
    // 设置每一列的宽度
    ui->tw_comColors->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    //    ui->tw_comColors->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tw_comColors->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tw_comColors->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

    ui->tw_comColors->setHorizontalHeaderLabels(headers);
    ui->tw_comColors->verticalHeader()->setVisible(false);
    for (int i = 0; i < cct.size(); i++) {
        ui->tw_comColors->setItem(i, 0, new QTableWidgetItem(cct[i]));
        tmp = new QTableWidgetItem();
        tmp->setBackground(colorList[i]);
        tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
        ui->tw_comColors->setItem(i, 1, tmp);
        ui->tw_comColors->setItem(i, 2, new QTableWidgetItem(MYUTILS::converRGBStr(colorList[i])));
        ui->tw_comColors->setItem(i, 3, new QTableWidgetItem(MYUTILS::converRGB16HexStr(colorList[i])));
    }

    ////////////// 初始化prob colors //////////////////////
    headers = QStringList({"PROBABILITY_COLOR", "low", "upper", "color", "rgb", "hex", "operation"});
    ui->tw_proTable->setRowCount(0);
    ui->tw_proTable->setColumnCount(headers.size());
    // 设置每一列的宽度
    for (int i = 0; i < headers.size(); i++) {
        ui->tw_proTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }
    ui->tw_proTable->setHorizontalHeaderLabels(headers);
    ui->tw_proTable->verticalHeader()->setVisible(false);
    if (scen->getIsInited()) {
        QList<ProbablityColor> pcs = scen->getProbablityColors();
        for (int i = 0; i < pcs.size(); i++) {
            addProbColorsItem(pcs[i]);
        }
    }

    ////////////// 初始化landuse colors //////////////////////
    headers = QStringList({"LANDUSE_CLASS", "pix", "name", "flag", "color", "rgb", "hex", "operation"});
    QList<LandClass> landClass = scen->getLandCColors();
    ui->tw_landColorTable->setRowCount(0);
    ui->tw_landColorTable->setColumnCount(headers.size());
    // 设置每一列的宽度
    for (int i = 0; i < headers.size(); i++) {
        ui->tw_landColorTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }
    ui->tw_landColorTable->setHorizontalHeaderLabels(headers);
    ui->tw_landColorTable->verticalHeader()->setVisible(false);
    for (int i = 0; i < landClass.size(); i++) {
        addLandColorsItem(landClass[i]);
    }

    ////////////// 初始化growth colors //////////////////////
    headers            = QStringList({"PHASE_OF_GRWOTH", "color", "rgb", "hex"});
    QStringList phases = {"PHASE0G_GROWTH_COLOR", "PHASE1G_GROWTH_COLOR", "PHASE2G_GROWTH_COLOR",
        "PHASE3G_GROWTH_COLOR", "PHASE4G_GROWTH_COLOR", "PHASE5G_GROWTH_COLOR"};
    ui->tw_gColorTable->setRowCount(scen->getPGCs().size());
    ui->tw_gColorTable->setColumnCount(headers.size());
    // 设置每一列的宽度
    for (int i = 0; i < headers.size(); i++) {
        ui->tw_gColorTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }
    ui->tw_gColorTable->setHorizontalHeaderLabels(headers);
    ui->tw_gColorTable->verticalHeader()->setVisible(false);

    for (int i = 0; i < phases.size(); i++) {
        ui->tw_gColorTable->setItem(i, 0, new QTableWidgetItem(phases[i]));
        tmp      = new QTableWidgetItem();
        QColor c = scen->getPGCByIndex(i);
        tmp->setBackground(c);
        tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
        ui->tw_gColorTable->setItem(i, 1, tmp);
        ui->tw_gColorTable->setItem(i, 2, new QTableWidgetItem(MYUTILS::converRGBStr(c)));
        ui->tw_gColorTable->setItem(i, 3, new QTableWidgetItem(MYUTILS::converRGB16HexStr(c)));
    }

    ////////////// 初始化deltron colors //////////////////////
    headers = QStringList({"age", "color", "rgb", "hex", "operation"});
    ui->tw_dColorTable->setRowCount(6);
    ui->tw_dColorTable->setColumnCount(headers.size());
    // 设置每一列的宽度
    ui->tw_dColorTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    //    ui->tw_dColorTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tw_dColorTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tw_dColorTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->tw_dColorTable->setHorizontalHeaderLabels(headers);
    ui->tw_dColorTable->verticalHeader()->setVisible(false);
    for (int i = 0; i < scen->getDCs().size(); i++) {
        ui->tw_dColorTable->setItem(i, 0, new QTableWidgetItem(QString::number(i)));
        QPushButton *btnd = new QPushButton();
        tmp               = new QTableWidgetItem();
        QColor c          = scen->getDCByIndex(i);
        btnd->setText(QString::fromLocal8Bit("删除"));
        connect(btnd, SIGNAL(clicked()), this, SLOT(btn_delete_DItem_clicked()));
        tmp->setBackground(c);
        tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
        ui->tw_dColorTable->setItem(i, 1, tmp);
        ui->tw_dColorTable->setItem(i, 2, new QTableWidgetItem(MYUTILS::converRGBStr(c)));
        ui->tw_dColorTable->setItem(i, 3, new QTableWidgetItem(MYUTILS::converRGB16HexStr(c)));
        ui->tw_dColorTable->setCellWidget(i, 4, btnd);
    }
}

/**
 * @brief ScenarioForm::initImageTable
 * 初始化imageTable
 */
void ScenarioForm::initImageTable() {
    ui->tw_images->setRowCount(0);
    ui->tw_images->setColumnCount(3);
    // 设置每一列的宽度
    QStringList headers = {"ImageTypes", "Value", "operation"};
    ui->tw_images->setHorizontalHeaderLabels(headers);
    for (int i = 0; i < headers.size(); i++) {
        ui->tw_images->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }
    //    ui->tw_images->verticalHeader()->setVisible(false);  //隐藏列表头
    QStringList tmpList = scen->getUDS();
    for (QString fileName : tmpList) {
        addImageInputItem(fileName, "URBAN_DATA");
    }

    tmpList = scen->getRDS();
    for (QString fileName : tmpList) {
        addImageInputItem(fileName, "ROAD_DATA");
    }

    tmpList = scen->getLDS();
    for (QString fileName : tmpList) {
        addImageInputItem(fileName, "LANDUSE_DATA");
    }

    addImageInputItem(scen->getED(), "EXCLUDED_DATA");
    addImageInputItem(scen->getSD(), "SLOPE_DATA");
    addImageInputItem(scen->getBD(), "BACKGROUND_DATA");
}

////////////// 表格中的删除按钮事件 //////////////////
void ScenarioForm::deleteTableItem(QTableWidget *qwt) {
    QPushButton *button = dynamic_cast<QPushButton *>(QObject::sender());  //找到信号发送者
    QModelIndex  index  = qwt->indexAt(button->pos());                     //定位按钮
    qwt->removeRow(index.row());
}

void ScenarioForm::addProbColorsItem(ProbablityColor pc) {
    int          rowIndex   = ui->tw_proTable->rowCount();
    QPushButton *btn_delete = new QPushButton();
    btn_delete->setText(QString::fromLocal8Bit("删除"));
    connect(btn_delete, SIGNAL(clicked()), this, SLOT(btn_delete_probItem_clicked()));
    QTableWidgetItem *tmp = new QTableWidgetItem();
    QColor            c   = pc.color;
    tmp->setBackground(c);
    tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
    ui->tw_proTable->insertRow(rowIndex);
    ui->tw_proTable->setItem(rowIndex, 0, new QTableWidgetItem("PROBABILITY_COLOR"));
    ui->tw_proTable->setItem(rowIndex, 1, new QTableWidgetItem(QString::number(pc.low)));
    ui->tw_proTable->setItem(rowIndex, 2, new QTableWidgetItem(QString::number(pc.high)));
    ui->tw_proTable->setItem(rowIndex, 3, tmp);
    ui->tw_proTable->setItem(rowIndex, 4, new QTableWidgetItem(MYUTILS::converRGBStr(c)));
    ui->tw_proTable->setItem(rowIndex, 5, new QTableWidgetItem(MYUTILS::converRGB16HexStr(c)));
    ui->tw_proTable->setCellWidget(rowIndex, ui->tw_proTable->columnCount() - 1, btn_delete);
}

void ScenarioForm::addLandColorsItem(LandClass lc) {
    // 行标从0开始
    int          rowIndex   = ui->tw_landColorTable->rowCount();
    QPushButton *btn_delete = new QPushButton();
    QComboBox *  combox     = new QComboBox();
    combox->addItems(*flagTypes);
    combox->setCurrentText(lc.flag);
    btn_delete->setText(QString::fromLocal8Bit("删除"));
    connect(btn_delete, SIGNAL(clicked()), this, SLOT(btn_delete_landItem_clicked()));
    QTableWidgetItem *tmp = new QTableWidgetItem();
    QColor            c   = lc.color;
    tmp->setBackground(c);
    tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
    ui->tw_landColorTable->insertRow(rowIndex);
    ui->tw_landColorTable->setItem(rowIndex, 0, new QTableWidgetItem("LANDUSE_CLASS"));
    ui->tw_landColorTable->setItem(rowIndex, 1, new QTableWidgetItem(QString::number(lc.pix)));
    ui->tw_landColorTable->setItem(rowIndex, 2, new QTableWidgetItem(lc.name));
    ui->tw_landColorTable->setItem(rowIndex, 4, tmp);
    ui->tw_landColorTable->setItem(rowIndex, 5, new QTableWidgetItem(MYUTILS::converRGBStr(c)));
    ui->tw_landColorTable->setItem(rowIndex, 6, new QTableWidgetItem(MYUTILS::converRGB16HexStr(c)));
    ui->tw_landColorTable->setCellWidget(rowIndex, 3, combox);
    ui->tw_landColorTable->setCellWidget(rowIndex, ui->tw_landColorTable->columnCount() - 1, btn_delete);
}

void ScenarioForm::addImageInputItem(QString inImage, QString tname) {
    int          rowIndex   = ui->tw_images->rowCount();
    QPushButton *btn_delete = new QPushButton();
    QComboBox *  combox     = new QComboBox();
    combox->addItems(*imageTypes);
    combox->setEditText(tname);
    combox->setCurrentIndex(imageTypes->indexOf(tname));
    btn_delete->setText(QString::fromLocal8Bit("删除"));
    connect(btn_delete, SIGNAL(clicked()), this, SLOT(btn_delete_clicked()));
    ui->tw_images->insertRow(rowIndex);
    ui->tw_images->setCellWidget(rowIndex, 0, combox);
    ui->tw_images->setItem(rowIndex, 1, new QTableWidgetItem(inImage));
    ui->tw_images->setCellWidget(rowIndex, 2, btn_delete);
}

void ScenarioForm::btn_delete_clicked() {
    hasChanged();
    deleteTableItem(ui->tw_images);
}

void ScenarioForm::btn_delete_probItem_clicked() {
    hasChanged();
    deleteTableItem(ui->tw_proTable);
}

void ScenarioForm::btn_delete_landItem_clicked() {
    hasChanged();
    deleteTableItem(ui->tw_landColorTable);
}

void ScenarioForm::btn_delete_DItem_clicked() {
    hasChanged();
    deleteTableItem(ui->tw_dColorTable);
}

////////////// 表格中的按钮事件 //////////////////

/**
 * @brief ScenarioForm::colorCellClicked
 * 颜色单元格被点击后触发
 * 这里通过单元格所在列判断，列名为 color
 */
void ScenarioForm::colorCellClicked(QTableWidgetItem *item) {
    QTableWidget *qtw = item->tableWidget();
    int           row = item->row();
    int           col = item->column();
    // 只改变第二列
    if (qtw->horizontalHeaderItem(col)->text() != "color") {
        return;
    }
    QColor color = QColorDialog::getColor(Qt::white, this, "选择颜色");
    item->setBackground(color);
    item->setSelected(false);
    qtw->setItem(row, col + 1, new QTableWidgetItem(MYUTILS::converRGBStr(color)));
    qtw->setItem(row, col + 2, new QTableWidgetItem(MYUTILS::converRGB16HexStr(color)));
}

////////////// 向表格中添加一行的函数 //////////////
/**
 * @brief ScenarioForm::on_btn_addImage_clicked
 * 动态向表格添加一行
 */
void ScenarioForm::on_btn_addImage_clicked() { addImageInputItem("URBAN_DATA", ""); }

void ScenarioForm::tab1_save() {
    QFile file(scenarioPath);
    if (!file.open(QFile::ReadWrite | QFile::Text)) {
        QMessageBox::critical(this, "错误提示", "文件读取失败！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    try {
        QTextStream txtOutput(&file);
        txtOutput << ui->te_scenarioContents->toPlainText();
        QMessageBox::information(this, "提示", "文件保存成功！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    } catch (...) {
        QMessageBox::critical(this, "错误提示", "文件保存失败！", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    }

    file.close();
}

void ScenarioForm::on_btn_addProbColor_clicked() {
    ProbablityColor pc;
    addProbColorsItem(pc);
}

void ScenarioForm::on_btn_addLandColor_clicked() {
    LandClass lc;
    addLandColorsItem(lc);
}
////////////// 向表格中添加一行的函数 //////////////

void ScenarioForm::on_btn_addDColor_clicked() {
    // 行标从0开始
    int          rowIndex   = ui->tw_dColorTable->rowCount();
    QPushButton *btn_delete = new QPushButton();
    btn_delete->setText(QString::fromLocal8Bit("删除"));
    connect(btn_delete, SIGNAL(clicked()), this, SLOT(btn_delete_DItem_clicked()));
    QTableWidgetItem *tmp = new QTableWidgetItem();
    QColor            c   = QColor(200, 200, 200);
    tmp->setBackground(c);
    tmp->setFlags(tmp->flags() & (~Qt::ItemIsEditable));
    ui->tw_dColorTable->insertRow(rowIndex);
    ui->tw_dColorTable->setItem(rowIndex, 0, new QTableWidgetItem(QString::number(rowIndex)));
    ui->tw_dColorTable->setItem(rowIndex, 1, tmp);
    ui->tw_dColorTable->setItem(rowIndex, 2, new QTableWidgetItem(MYUTILS::converRGBStr(c)));
    ui->tw_dColorTable->setItem(rowIndex, 3, new QTableWidgetItem(MYUTILS::converRGB16HexStr(c)));
    ui->tw_dColorTable->setCellWidget(rowIndex, ui->tw_dColorTable->columnCount() - 1, btn_delete);
}

void ScenarioForm::on_btn_2sw1_clicked() { ui->stackedWidget_Scenario->setCurrentIndex(0); }

void ScenarioForm::on_btn_2sw2_clicked() { ui->stackedWidget_Scenario->setCurrentIndex(1); }

void ScenarioForm::on_btn_2sw3_clicked() { ui->stackedWidget_Scenario->setCurrentIndex(2); }

void ScenarioForm::on_btn_2sw4_clicked() { ui->stackedWidget_Scenario->setCurrentIndex(3); }

void ScenarioForm::on_btn_sinputDIr_clicked() {
    QSettings setting("./Setting.ini", QSettings::IniFormat);  //为了能记住上次打开的路径
    QString   lastPath = setting.value("LastFilePath").toString();
    QString   tmp      = QFileDialog::getExistingDirectory(this, "请选择文件路径...", lastPath);
    if (tmp == "") {
        return;
    }
    ui->le_sinputDir->setText(MYUTILS::getRelativeDirPath(tmp));
}

void ScenarioForm::hasChanged() { changed = true; }

void ScenarioForm::on_btn_soutputDir_clicked() {
    QSettings setting("./Setting.ini", QSettings::IniFormat);  //为了能记住上次打开的路径
    QString   lastPath = setting.value("LastFilePath").toString();
    QString   tmp      = QFileDialog::getExistingDirectory(this, "请选择文件路径...", lastPath);
    if (tmp == "") {
        return;
    }
    ui->le_soutputDIr->setText(MYUTILS::getRelativeDirPath(tmp));
}

void ScenarioForm::on_btn_sWhiBin_clicked() {
    QSettings setting("./Setting.ini", QSettings::IniFormat);  //为了能记住上次打开的路径
    QString   lastPath = setting.value("LastFilePath").toString();
    QString   tmp      = QFileDialog::getExistingDirectory(this, "请选择文件路径...", lastPath);
    if (tmp == "") {
        return;
    }
    ui->le_sWhBin->setText(MYUTILS::getRelativeDirPath(tmp));
}
