﻿#pragma once
#pragma execution_character_set("utf-8")
#include <qftp.h>
#include <sleuthlib/scenarioStruct.h>

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QMap>
#include <QMessageBox>
#include <QRegExp>
#include <QSize>
#include <QStyleFactory>
#include <QTextCodec>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("GBK"));
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setStyle(QStyleFactory::create("Fusion"));
#endif
    QApplication a(argc, argv);
    MainWindow   w;
    w.show();
    return a.exec();
}
